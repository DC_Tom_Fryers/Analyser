"""
This module creates the Flask web app.

Use `analyser.ui.create_app` to create a Flask app object.
"""
from __future__ import annotations

import csv
import html
from base64 import b85decode
from collections.abc import Callable, Container, Iterable, Mapping
from functools import wraps
from io import StringIO
from typing import Any, cast

from flask import (
    Flask,
    Response,
    abort,
    flash,
    redirect,
    render_template,
    request,
    session,
)
from flask_session import Session
from markupsafe import Markup

import analyser.analysis
import analyser.analysis.default
import analyser.dataset_summary
import analyser.load_file
import analyser.script_edit
import analyser.script_edit.script_slicing
from analyser import scripts
from analyser.html_gen import tag
from analyser.scripts.errors import ScriptError
from analyser.table_to_html import table_to_html


def returns_csv(
    filename: str,
) -> Callable[[Callable[..., Iterable[Iterable[object]]]], Callable[..., Response]]:
    """
    Turn an iterable of iterables return type into a CSV response.

    The name of the CSV file can contain formatting placeholders. These
    will be replaced with the values of the correspondingly keyword
    arguments when the function is called.
    """

    def decorator(
        function: Callable[..., Iterable[Iterable[object]]]
    ) -> Callable[..., Response]:
        @wraps(function)
        def inner(*args: Any, **kwargs: Any) -> Response:
            value = function(*args, **kwargs)
            with StringIO() as s:
                csv.writer(s).writerows(value)
                csv_ = s.getvalue()
            return Response(
                csv_,
                mimetype="text/csv;charset=UTF-8",
                headers={
                    "Content-Disposition": (
                        f"attachment; filename={filename.format_map(kwargs)}.csv"
                    )
                },
            )

        return inner

    return decorator


def english_number(number: int, /) -> str:
    """
    Get the name of an integer, for use in written text.

    This follows the convention of writing out numbers less than 10.
    """
    if number < 10:
        return (
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
        )[number]
    return str(number)


FUNCTIONS: tuple[Callable[..., Any], ...] = (zip, english_number)


def safe_script(script: str, /) -> scripts.nodes.Script:
    """
    Safely parse a script.

    If parsing fails, the user is redirected back to the setup page and
    shown a message with their error.
    """
    try:
        script_node = scripts.parse.parse_script(script)
        session["script"] = script
        return script_node
    except ScriptError as error:
        flash(str(error))
        abort(redirect("/setup"))


def error_message(script: str, error: scripts.errors.ScriptError) -> str | None:
    """Create an error message highlighting the location of an error."""
    try:
        whole = error.error_stack[-1].pos
        part = error.error_stack[0].pos
    except (AttributeError, IndexError):
        return None
    return (
        script[whole.start : part.start]
        + tag.span(script[part], class_="error")
        + script[part.stop : whole.stop]
    )


def get_analysis(
    script_node: scripts.nodes.Script,
) -> tuple[
    tuple[str, ...] | None, tuple[tuple[tuple[str, ...], ...], ...], tuple[str, ...]
]:
    """
    Run an analysis script on the uploaded script.

    If an error is raised, the user is redirected to the setup page.
    """
    data = session["data"]
    try:
        return analyser.analysis.analyse(data, script_node)
    except ScriptError as error:
        flash(str(error))
        detail = error_message(session["script"], error)
        if detail is not None:
            flash(detail)
        abort(redirect("/setup"))


# These are different because there is little value in seeing a huge
# number of values once it can't be all of them, and the slowdown is
# significant.
MAX_ROWS = 3000
ABOVE_MAX_ROW_COUNT = 1000


def default_sort(
    headings: Iterable[str], new: Container[str], used: Container[str]
) -> int:
    """
    Determine the table column to sort by by default.

    This is the last column that is both user-created and used in the
    current table.

    :return: The index of the column.
    :raises StopIteration: No default is found.
    """
    return next(
        i for i, h in reversed(tuple(enumerate(headings))) if h in new and h in used
    )


def create_app(session_data_path: str = "session_data") -> Flask:
    """Create a Flask app object."""
    app = Flask(__name__)
    app.secret_key = b85decode("wc}hzsn_3aP25J>Iyh{p")
    app.config["TEMPLATES_AUTO_RELOAD"] = True
    app.config["SESSION_TYPE"] = "filesystem"
    app.config["SESSION_FILE_DIR"] = session_data_path
    app.config["SESSION_COOKIE_SAMESITE"] = "Strict"
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True
    app.debug = True
    # import flask_debugtoolbar

    # app.config["DEBUG_TB_PANELS"] = [
    #     "flask_debugtoolbar.panels.timer.TimerDebugPanel",
    #     "flask_debugtoolbar.panels.headers.HeaderDebugPanel",
    #     "flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel",
    #     "flask_debugtoolbar.panels.template.TemplateDebugPanel",
    #     "flask_debugtoolbar.panels.profiler.ProfilerDebugPanel",
    # ]
    # flask_debugtoolbar.DebugToolbarExtension(app)
    Session(app)

    @app.context_processor
    def send_functions() -> dict[str, Callable[..., Any]]:
        return {f.__name__: f for f in FUNCTIONS}

    with app.app_context():
        HELP = render_template(
            "help.html.jinja",
            buttons=analyser.script_edit.BUTTONS.values(),
            button=analyser.script_edit.fake_button,
            common_count=analyser.dataset_summary.ROWS,
        )

    @app.route("/")
    def index() -> str:
        """Return the landing page."""
        return render_template("index.html.jinja")

    @app.route("/upload")
    def upload() -> str:
        """Return the data upload page."""
        return render_template(
            "upload.html.jinja",
            filetypes=tag.ol(
                "".join(f.description for f in analyser.load_file.FILETYPES)
            ),
        )

    @app.route("/setup", methods=["POST", "GET"])
    def setup() -> str:
        """Return the column assignment page."""
        actions = None
        if request.method == "POST":
            if request.form:
                # Request came from editing the script.
                actions = request.form
                try:
                    script = analyser.script_edit.script_slicing.modify_script(
                        actions["script"].replace("\r\n", "\n"),
                        [(a, v) for a, v in actions.items() if a != "script"],
                    )
                except ValueError:
                    flash("Overlapping edits")
                    abort(redirect("/setup"))
            else:
                # Request came from the file upload page.
                data_file = request.files["file"]
                try:
                    filename = data_file.filename
                    if filename is None:
                        raise ValueError("no filename")
                    dataframe = analyser.load_file.load(data_file.stream, filename)
                except Exception:
                    flash("Invalid file")
                    abort(redirect("/upload"))
                session["summary"] = analyser.dataset_summary.summary(dataframe)
                script = analyser.analysis.default.script(dataframe)
                data = {
                    c: tuple(s)
                    for c, s in cast(
                        "Mapping[str, Iterable[str]]", dataframe.to_dict("series")
                    ).items()
                }
                session["data"] = data
                session["columns"] = tuple(data)
        else:
            script = session["script"]
        script = script.replace("\r\n", "\n")
        script_node = safe_script(script)
        script_edit = analyser.script_edit.script_to_html(
            script_node, session["columns"], analyser.analysis.AVAILABLE_NAMES
        )

        return render_template(
            "setup.html.jinja",
            help=HELP,
            summary=session["summary"],
            script=session["script"],
            script_edit=script_edit,
            submit_blocked='<input list="variables"' in script_edit,
        )

    @app.route("/import-upload", methods=["POST"])
    def import_() -> None:
        """Handle an uploaded script."""
        with request.files["file"].stream as f:
            script = f.read().decode("utf-8")
        safe_script(script)
        abort(redirect("/setup"))

    @app.route("/export", methods=["POST"])
    def export() -> Response:
        """Provide the current script as a download."""
        filename = request.form["filename"]
        if not filename:
            filename = "analysis"
        filename = f"{filename}.txt"
        return Response(
            session["script"],
            mimetype="text/plain;charset=UTF-8",
            headers={"Content-Disposition": f"attachment; filename={filename}"},
        )

    @app.route("/analysis", methods=["POST"])
    def analysis() -> str:
        """Return the final analysis page."""
        script = request.form["script"]
        session["script"] = script
        script_node = safe_script(request.form["script"])
        statistics, results, new_columns = get_analysis(script_node)
        page_names = []
        pages = []
        descriptions = []
        if statistics is not None:
            page_names.append("statistics")
            pages.append("".join(tag.p(s) for s in statistics))
            descriptions.append(
                analyser.script_edit.get_expression_dependencies(
                    script_node,
                    analyser.script_edit.find_definition(
                        script_node, "statistics"
                    ).expression,
                    "statistics",
                    analyser.analysis.AVAILABLE_NAMES,
                )[0]
            )
        if results:
            positions, page_descriptions, used_columns = zip(
                *(
                    analyser.script_edit.get_page_definition(
                        script_node, analyser.analysis.AVAILABLE_NAMES, i
                    )
                    for i in range(len(results))
                )
            )
            descriptions += list(page_descriptions)
            page_names += [
                html.escape(script[p]) if p is not None else f"page {i}"
                for i, p in enumerate(positions)
            ]
            counts = [len(i) - 1 for i in results]
            for page, used, count in zip(results, used_columns, counts):
                if count > MAX_ROWS:
                    prefix = tag.p(
                        "There are too many results to show in the HTML table so"
                        f" only the first {ABOVE_MAX_ROW_COUNT} are shown here."
                        " Download the CSV for the full set."
                    )
                    page = page[: ABOVE_MAX_ROW_COUNT + 1]
                else:
                    prefix = ""
                if count == 0:
                    pages.append(tag.p("No results found."))
                    continue
                try:
                    sort_column = default_sort(page[0], new_columns, used)
                except StopIteration:
                    sort_column = -1
                column_classes: list[list[str]] = []
                for i, column in enumerate(page[0]):
                    classes = []
                    if column in new_columns:
                        classes.append("new_column")
                    if column in used:
                        classes.append("used_column")
                    if i == sort_column:
                        classes.append("initial_sort")
                    column_classes.append(classes)

                pages.append(
                    prefix
                    + table_to_html(
                        page, column_classes, classes=("analysis_table", "sortable")
                    )
                )
        all_counts: list[int | None] = [] if statistics is None else [None]
        if results:
            all_counts += counts
        return render_template(
            "analysis.html.jinja",
            page_names=page_names,
            pages=[Markup(t) for t in pages],
            counts=all_counts,
            descriptions=descriptions,
        )

    @app.route("/analysis/<int:page>.csv")
    @returns_csv("{page}")
    def analysis_csv(page: int) -> tuple[tuple[str, ...], ...]:
        """Return an exported CSV."""
        return get_analysis(safe_script(session["script"]))[1][page]

    return app
