from __future__ import annotations

import string
from collections.abc import Callable, Collection, Iterable
from datetime import date, time
from typing import Any

import pandas as pd

from analyser import dates, scripts, times
from analyser.scripts.functions import Function

date_type = scripts.types.Primitive("Date")
time_type = scripts.types.Primitive("Time")
group_id_type = scripts.types.Primitive("GroupID")


def get_string_implementation(type_: scripts.types.Type) -> Callable[[Any], str] | None:
    """
    Find a function for converting a given type to a string.

    Looks through the Representable typeclass to find an implementation,
    returning None if there isn't one.
    """
    for impl in (
        scripts.builtin.CLASSES["Representable"].implementations,
        CLASS_IMPLEMENTATIONS["Representable"][1],
    ):
        if type_ in impl:
            return impl[type_]["string"].value.raw_function
    return None


def get_final(
    variables: dict[str, scripts.types.Variable], original_columns: Collection[str]
) -> tuple[tuple[tuple[str, ...], ...], tuple[str, ...]]:
    """
    Produce an output table with all variables as columns.

    :return: A big table and a tuple of names of new variables.
    """
    values = {}
    types = {}
    for name, variable in variables.items():
        if (
            isinstance(variable.type_, scripts.types.Array)
            and not name.startswith("_")
            and len(variable.value)
            == len(variables[next(iter(original_columns))].value)
        ):
            values[name] = variable.value
            types[name] = variable.type_
    result = dict(sorted(values.items(), key=lambda x: x[0] in original_columns))
    for name, type_ in types.items():
        function = get_string_implementation(type_.types[0])
        if function is None:
            if isinstance(result[name][0], str):
                continue
            function = str
        result[name] = tuple(map(function, result[name]))

    return (tuple(result),) + tuple(zip(*result.values())), tuple(
        n for n in values if n not in original_columns
    )


def match(columns: Iterable[tuple[str, ...]], /) -> tuple[int, ...]:
    series = [pd.Series(c) for c in columns]
    if not series:
        return ()
    dataframe = pd.concat(series, axis=1, copy=False)
    groups = dataframe.groupby(list(dataframe.columns))[0]
    ids = groups.ngroup()
    ids[groups.transform("size") == 1] = -1
    return tuple(ids)


def dups(column: tuple[int, ...], /) -> tuple[bool, ...]:
    return tuple(map(lambda x: x != -1, column))


def parse_date(value: str, /) -> tuple[date] | None:
    try:
        return (dates.to_date(value),)
    except ValueError:
        return None


def parse_time(value: str, /) -> tuple[time] | None:
    try:
        return (times.to_time(value),)
    except ValueError:
        return None


# TODO Add more functions.
FUNCTIONS: dict[str, scripts.types.Variable] = {
    "groups": Function.new(
        (
            scripts.types.Array(
                scripts.types.Array(scripts.types.T.bound("Equatable")[0])
            ),
        ),
        scripts.types.Array(group_id_type),
        match,
    ),
    "digits_only": Function.new(
        (scripts.types.string,),
        scripts.types.string,
        lambda n, /: "".join(i for i in n if i.isnumeric()),
    ),
    "letters_only": Function.new(
        (scripts.types.string,),
        scripts.types.string,
        lambda n, /: "".join(i for i in n if i.isalpha()),
    ),
    "begins_with": Function.new(
        (scripts.types.string, scripts.types.string),
        scripts.types.boolean,
        lambda n, h, /: h.startswith(n),
    ),
    "ends_with": Function.new(
        (scripts.types.string, scripts.types.string),
        scripts.types.boolean,
        lambda n, h, /: h.endswith(n),
    ),
    "year": Function.new(
        (date_type,), scripts.types.integer, date.year.__get__  # type: ignore[attr-defined]
    ),
    "month": Function.new(
        (date_type,), scripts.types.integer, date.month.__get__  # type: ignore[attr-defined]
    ),
    "day": Function.new(
        (date_type,), scripts.types.integer, date.day.__get__  # type: ignore[attr-defined]
    ),
    "week": Function.new(
        (date_type,), scripts.types.integer, lambda x, /: x.isocalendar().week
    ),
    "weekday": Function.new((date_type,), scripts.types.integer, date.isoweekday),
    "hour": Function.new(
        (time_type,), scripts.types.integer, time.hour.__get__  # type: ignore[attr-defined]
    ),
    "minute": Function.new(
        (time_type,), scripts.types.integer, time.minute.__get__  # type: ignore[attr-defined]
    ),
    "second": Function.new(
        (time_type,), scripts.types.integer, time.second.__get__  # type: ignore[attr-defined]
    ),
    "duplicates": Function.new(
        (scripts.types.Array(group_id_type),),
        scripts.types.Array(scripts.types.boolean),
        dups,
    ),
}
PRELUDE = scripts.parse.parse_script(
    """
array_sum: a* -> ? [] (reduce (zip_with +) a*)
    """
)
AVAILABLE_NAMES = tuple(
    sorted(scripts.builtin.BUILTIN_NAMES.union(FUNCTIONS) | {"array_sum"})
)
CLASS_IMPLEMENTATIONS: dict[
    str,
    tuple[
        frozenset[str] | None,
        dict[scripts.types.Type, dict[str, scripts.types.Variable]],
    ],
] = {
    "Representable": (
        frozenset(),
        {
            date_type: {
                "string": Function.new(
                    (date_type,), scripts.types.string, date.isoformat
                ),
                "parse": Function.new(
                    (scripts.types.string,), scripts.types.Option(date_type), parse_date
                ),
            },
            time_type: {
                "string": Function.new(
                    (time_type,), scripts.types.string, time.isoformat
                ),
                "parse": Function.new(
                    (scripts.types.string,), scripts.types.Option(time_type), parse_time
                ),
            },
            group_id_type: {
                "string": Function.new(
                    (group_id_type,),
                    scripts.types.string,
                    lambda x: str(x) if x != -1 else "",
                ),
                "parse": Function.new(
                    (scripts.types.string,),
                    scripts.types.Option(group_id_type),
                    lambda x: (int(x) if x else -1,)
                    if set(x).issubset(string.digits)
                    else None,
                ),
            },
        },
    )
}


def var_exists_with_type(
    variables: dict[str, scripts.types.Variable],
    variable_name: str,
    type_: scripts.types.Type,
) -> bool:
    """
    Check if a variable has been defined.

    If the variable has been defined, its type must match the given one.

    :raises scripts.errors.ScriptTypeError: The types do not match.
    """
    try:
        if variables[variable_name].type_ == type_:
            return True
        raise scripts.errors.ScriptTypeError(f"{variable_name} must have_type {type_}.")
    except KeyError:
        return False


def analyse(
    data: dict[str, tuple[str, ...]], script: scripts.nodes.Script
) -> tuple[
    tuple[str, ...] | None, tuple[tuple[tuple[str, ...], ...], ...], tuple[str, ...]
]:
    """
    Analyse a dataset using a script.

    :return:
        A tuple of three items. The first gives the 'statistics'.
        The second and third give the filtered tables and their
        corresponding names, respectively.
    """
    new_vars = scripts.run(
        script,
        PRELUDE,
        additional_variables={
            **FUNCTIONS,
            **{
                c: scripts.types.Variable(scripts.types.Array(scripts.types.string), s)
                for c, s in data.items()
            },
        },
        additional_classes=CLASS_IMPLEMENTATIONS,
    )
    # | doesn't short-circuit, unlike or
    if not (
        var_exists_with_type(
            new_vars,
            "pages",
            scripts.types.Array(scripts.types.Array(scripts.types.boolean)),
        )
        | var_exists_with_type(
            new_vars, "statistics", scripts.types.Array(scripts.types.string)
        )
    ):
        raise scripts.errors.ScriptNameError("No statistics or pages.") from None
    result_table, new_columns = get_final(new_vars, tuple(data))
    return (
        new_vars["statistics"].value if "statistics" in new_vars else None,
        tuple(
            tuple(
                row
                for row, is_included in zip(result_table, (True,) + page)
                if is_included
            )
            for page in new_vars["pages"].value
        )
        if "pages" in new_vars
        else (),
        new_columns,
    )
