import pandas as pd


def script(data: pd.DataFrame) -> str:
    """Generate a default analysis script."""
    return "pages: []"
