from __future__ import annotations

from math import log10

import pandas as pd

from analyser.html_gen import tag

ROWS = 4
PREFIXES = ("",) + tuple("kMGTPEZY")


def memory_format(bytes_: int, /) -> str:
    prefix = int(log10(bytes_) / 3)
    if round(bytes_ / 1000 ** prefix) == 1000:
        prefix += 1
    return f"{round(bytes_ / 1000 ** prefix)} {PREFIXES[prefix]}B"


def summary(data: pd.DataFrame) -> str:
    most_frequent = {
        column: data[column].value_counts()[:ROWS] for column in data.columns
    }

    tables = "".join(
        tag.table(
            tag.thead(tag.tr(tag.th(name, colspan="2")))
            + tag.tbody(
                "".join(
                    tag.tr(tag.td(tag.em(value)) + tag.td(count))
                    for value, count in zip(table.index, table)
                )
            )
        )
        for name, table in most_frequent.items()
    )
    return (
        tag.h3("Data Preview")
        + tag.p(
            f"{len(data.columns)} columns × {len(data)} rows"
            f" ({memory_format(data.memory_usage().sum())})."
        )
        + tag.div(tables, class_="summary_table")
    )
