"""This module provides various string processing functions."""
from __future__ import annotations


def removeprefix(string: str, prefix: str) -> str:
    if prefix and string.startswith(prefix):
        return string[len(prefix) :]
    return string


def removesuffix(string: str, suffix: str) -> str:
    if suffix and string.endswith(suffix):
        return string[: -len(suffix)]
    return string


def do_replacements(script: str, replacements: list[tuple[slice, str]]) -> str:
    """Substitute new values into the script string."""
    replacements.sort(key=lambda x: x[0].stop)
    if not all(x[0].stop < y[0].start for x, y in zip(replacements, replacements[1:])):
        raise ValueError("replacements can not overlap")
    for pos, value in reversed(replacements):
        script = script[: pos.start] + value + script[pos.stop :]
    return script


def find_comma(script: str, start: int, direction: int = 1) -> int:
    """Find how far back is whitespace starting with an optional single comma."""
    while script[start + direction] in " \n\t":
        start += direction
    if script[start + direction] == ",":
        start += direction
    return start


def needs_brackets(script: str, pos: slice) -> bool:
    return next(c for c in script[pos.start - 1 :: -1] if c not in " \t") not in "[(,:"


BRACKETS = ("()", "[]", "{}")


def in_brackets(string: str, /, brackets: str = "(") -> bool:
    """Determine if a string is surrounded by brackets."""
    return any(
        set(bracket_set).intersection(brackets)
        and (string[0] + string[-1]) == bracket_set
        for bracket_set in BRACKETS
    )


def bracket(string: str, /, brackets: str = "(") -> str:
    for open_bracket in brackets:
        close_bracket = next(b[1] for b in BRACKETS if b[0] == open_bracket)
        string = f"{open_bracket}{string}{close_bracket}"
    return string
