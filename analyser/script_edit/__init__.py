"""This module provides functions for letting users modify scripts."""
from __future__ import annotations

import html
import re
from collections.abc import Callable, Container, Iterable
from dataclasses import dataclass
from functools import cached_property
from typing import TYPE_CHECKING, Any, TypeVar

from analyser import scripts
from analyser.html_gen import tag
from analyser.script_edit import strings


def clamp(value: int, minimum: int, maximum: int) -> int:
    return max(min(value, maximum), minimum)


def fake_button(button: str) -> str:
    return re.sub(
        r' name=".*?@"| type="submit"', "", BUTTONS[button].button(position="")
    )


def mini_buttons(buttons: tuple[str, ...], position: str) -> str:
    buttons_html = "".join(BUTTONS[b].button(position) for b in buttons)
    return tag.span(tag.span(buttons_html), class_="minibuttons")


def nice_symbol(symbol: str) -> str:
    return {
        "<=": "\N{LESS-THAN OR EQUAL TO}",
        ">=": "\N{GREATER-THAN OR EQUAL TO}",
        "/=": "\N{NOT EQUAL TO}",
        "->": "\N{RIGHTWARDS SANS-SERIF ARROW}",
        "*": "\N{MULTIPLICATION SIGN}",
        "-": "\N{MINUS SIGN}",
        "/": f"{NBSP}\N{DIVISION SLASH}{NBSP}",
    }.get(symbol, symbol)


# Used because HTML flex boxes eat normal spaces.
NBSP = "\N{NO-BREAK SPACE}"


@dataclass(frozen=True)
class Button:
    name: str
    symbol: str
    display_name: str

    def button(self, position: str) -> str:
        return self.template.format(position=position)

    @cached_property
    def template(self) -> str:
        return tag.button(
            self.symbol,
            title=self.display_name,
            class_="minibutton",
            type="submit",
            name=f"{self.name}@{{position}}",
        )


BUTTONS = {
    b.name: b
    for b in (
        Button("show", "\N{MIDLINE HORIZONTAL ELLIPSIS}", "Show section"),
        Button("hide", "\N{VERTICAL ELLIPSIS}", "Hide section"),
        Button("remove_statement", "\N{CANCELLATION X}", "Delete"),
        Button("add_statement_above", "+\N{UPWARDS ARROW}", "Add above"),
        Button("add_statement_below", "+\N{DOWNWARDS ARROW}", "Add below"),
        Button("duplicate_statement", "\N{TWO JOINED SQUARES}", "Duplicate"),
        Button("edit_identifier", "\N{LOWER RIGHT PENCIL}", "Edit name"),
        Button("remove", "\N{CANCELLATION X}", "Remove function"),
        Button("add_function_call", '<em class="serif">f</em>', "Apply function"),
        Button("map_function", "\N{THREE RIGHTWARDS ARROWS}", "Map function"),
        Button("add_array", "[ ]", "Add array"),
        Button("add_tuple", "(,)", "Add tuple"),
        Button(
            "add_function",
            '<em class="serif">\N{GREEK SMALL LETTER LAMDA}</em>',
            "Add function",
        ),
        Button("extend", "+", "Add item"),
        Button("shrink", "\N{ERASE TO THE LEFT}", "Remove item"),
        Button("edit", "\N{LOWER RIGHT PENCIL}", "Edit"),
    )
}


def pos(node: scripts.nodes.Node) -> str:
    return f"{node.pos.start}-{node.pos.stop}"


_N = TypeVar("_N", bound=scripts.nodes.Node)
if TYPE_CHECKING:
    NodeFormatter = tuple[
        type[_N], Callable[[Formatter, _N, Container[str]], str]  # noqa: F821
    ]


@dataclass
class Formatter:
    builtins: Container[str]
    editable: bool

    def __post_init__(self) -> None:
        self.depth = 0

    def add_edit(self, text: str, position: str, only_identifier: bool = False) -> str:
        if self.editable:
            edit_mode = "edit_identifier" if only_identifier else "edit"
            return text + mini_buttons((edit_mode,), position)
        return text

    def format_script(self, node: scripts.nodes.Script, flags: Container[str]) -> str:
        has_one_statement = (
            sum(isinstance(s, scripts.nodes.Assignment) for s in node.statements) == 1
        )
        lines = []
        hiding = False
        for statement in node.statements:
            if isinstance(statement, scripts.nodes.Comment) and (
                statement.content.endswith("[show]"),
                statement.content.endswith("[hide]"),
            ):
                hiding = False
            if not hiding:
                lines.append(
                    self.format_node(
                        statement, {"is_only_statement"} if has_one_statement else {}
                    )
                )
            if isinstance(statement, scripts.nodes.Comment):
                if statement.content.endswith("[show]"):
                    hiding = True
                elif statement.content.endswith("[end]"):
                    hiding = False
        return tag.input(type="submit", hidden="") + tag.div(
            "".join(lines), class_="code_view"
        )

    def format_assignment(
        self, node: scripts.nodes.Assignment, flags: Container[str]
    ) -> str:
        if self.editable:
            buttons_code = mini_buttons(
                (() if "is_only_statement" in flags else ("remove_statement",))
                + ("add_statement_above", "add_statement_below")
                + ("duplicate_statement",),
                pos(node),
            )
        else:
            buttons_code = ""

        return tag.div(
            buttons_code + self.format_node(node.identifier, {"only_identifier"}) + ":",
            class_="assignment_name",
        ) + tag.div(
            self.format_node(node.expression, {"needs_no_brackets"}),
            class_="assignment_value",
        )

    def format_comment(self, node: scripts.nodes.Comment, flags: Container[str]) -> str:
        if self.editable:
            buttons_code = mini_buttons(("remove_statement",), pos(node))
        else:
            buttons_code = ""
        content = node.content
        if node.content.endswith("[hide]"):
            content = content.replace("[hide]", mini_buttons(("hide",), pos(node)))
        if node.content.endswith("[show]"):
            content = content.replace("[show]", mini_buttons(("show",), pos(node)))
        return tag.div(
            buttons_code + tag.span(f"#{content}", class_="comment")
        ) + tag.div("")

    def format_identifier(
        self, node: scripts.nodes.Identifier, flags: Container[str]
    ) -> str:
        # Identifier-only text-input box
        if node.name.startswith("___") and self.editable:
            return tag.input(
                name=f"new@{pos(node)}", value=strings.removeprefix(node.name, "___")
            )
        text = nice_symbol(node.name)
        if node.name in self.builtins:
            text = tag.span(html.escape(text), class_="builtin")
        return self.add_edit(text, pos(node), "only_identifier" in flags)

    def format_number(
        self, node: scripts.nodes.Integer | scripts.nodes.Float, flags: Container[str]
    ) -> str:
        return self.add_edit(
            tag.span(str(node.value), class_="number"),
            pos(node),
            "only_identifier" in flags,
        )

    def format_string(self, node: scripts.nodes.String, flags: Container[str]) -> str:
        # General text-input box
        if node.value.startswith("___") and self.editable:
            text = strings.removeprefix(node.value, "___")
            text = tag.input(
                list="variables",
                name=f"new@{pos(node)}",
                value=text,
                size=str(clamp(len(text), 20, 90)),
            )
            buttons = [
                "add_function_call",
                "map_function",
                "add_array",
                "add_tuple",
                "add_function",
            ]
            if "is_a_function" in flags:
                buttons.insert(0, "remove")
            # Arrays and tuples can be shortened.
            if "is_in_array" in flags:
                buttons.append("shrink")
            return text + mini_buttons(tuple(buttons), pos(node))
        escaped = html.escape(scripts.parse.escape(node.value))
        return self.add_edit(
            tag.span(
                "\N{LEFT DOUBLE QUOTATION MARK}"
                + escaped
                + "\N{RIGHT DOUBLE QUOTATION MARK}",
                class_="string",
            ),
            pos(node),
        )

    def format_array_or_tuple(
        self, node: scripts.nodes.ArrayTuple, flags: Container[str]
    ) -> str:
        buttons_code = mini_buttons(("extend",), pos(node))
        items = f",{NBSP}".join(
            self.sub_format(n, {"is_in_array", "needs_no_brackets"})
            for n in node.values
        )
        text = items + (buttons_code if self.editable else "")
        text = strings.bracket(
            text, "[" if isinstance(node, scripts.nodes.Array) else "("
        )
        return self.add_edit(text, pos(node))

    def format_function(
        self, node: scripts.nodes.Function, flags: Container[str]
    ) -> str:
        text = NBSP.join(
            (
                self.format_node(node.parameter, {"only_identifier"}),
                nice_symbol("->"),
                self.sub_format(node.body, {"needs_no_brackets"}),
            )
        )
        if "needs_no_brackets" not in flags:
            text = strings.bracket(text)
        return self.add_edit(text, pos(node))

    def format_function_call(
        self, node: scripts.nodes.FunctionCall, flags: Container[str]
    ) -> str:
        func = self.sub_format(node.function, {"is_a_function"})
        arg = self.sub_format(node.argument, {"is_an_argument"})
        text = func + NBSP + arg
        if "is_an_argument" in flags:
            text = strings.bracket(text)
        return self.add_edit(text, pos(node))

    def format_attribute_access(
        self, node: scripts.nodes.AttributeAccess, flags: Container[str]
    ) -> str:
        bracketing = not isinstance(
            node.value,
            (scripts.nodes.Identifier, scripts.nodes.Literal, scripts.nodes.ArrayTuple),
        )
        func = self.sub_format(node.value, {"needs_no_brackets"} if bracketing else ())
        attr = self.sub_format(node.attribute, {"only_identifier"})
        if bracketing:
            func = strings.bracket(func)
        return self.add_edit(f"{func}{'..' if node.part else '.'}{attr}", pos(node))

    def format_if_then_else(
        self, node: scripts.nodes.IfThenElse, flags: Container[str]
    ) -> str:
        text = NBSP.join(
            (
                tag.span("if", class_="builtin"),
                self.sub_format(node.cond),
                tag.span("then", class_="builtin"),
                self.sub_format(node.true_val),
                tag.span("else", class_="builtin"),
                self.sub_format(node.false_val),
            )
        )
        if "needs_no_brackets" in flags:
            text = strings.bracket(text)
        return self.add_edit(text, pos(node))

    format_functions: tuple[NodeFormatter[Any], ...] = (
        (scripts.nodes.Script, format_script),
        (scripts.nodes.Assignment, format_assignment),
        (scripts.nodes.Comment, format_comment),
        (scripts.nodes.Identifier, format_identifier),
        (scripts.nodes.Integer, format_number),
        (scripts.nodes.Float, format_number),
        (scripts.nodes.String, format_string),
        (scripts.nodes.ArrayTuple, format_array_or_tuple),
        (scripts.nodes.Function, format_function),
        (scripts.nodes.FunctionCall, format_function_call),
        (scripts.nodes.AttributeAccess, format_attribute_access),
        (scripts.nodes.IfThenElse, format_if_then_else),
    )

    def format_node(self, node: scripts.nodes.Node, flags: Container[str] = ()) -> str:
        return next(f for t, f in self.format_functions if isinstance(node, t))(
            self, node, flags
        )

    def sub_format(self, value: scripts.nodes.Node, flags: Container[str] = ()) -> str:
        """Format a node with the next level of background colouring."""
        self.depth += 1
        inner = self.format_node(value, flags)
        self.depth -= 1
        return tag.span(
            inner, class_="shaded " + ("even_depth", "odd_depth")[self.depth % 2]
        )


def make_suggestion_list(suggestions: Iterable[str], id_: str) -> str:
    return tag.datalist("".join(tag.option("", value=s) for s in suggestions), id=id_)


def script_to_html(
    script: scripts.nodes.Script, variables: tuple[str, ...], builtins: tuple[str, ...]
) -> str:
    return make_suggestion_list(
        variables
        + tuple(
            n.identifier.name
            for n in script.statements
            if isinstance(n, scripts.nodes.Assignment)
        )
        + builtins,
        "variables",
    ) + Formatter(builtins, editable=True).format_node(script)


def used_definitions(
    definitions: dict[str, scripts.nodes.Expression], node: scripts.nodes.Node
) -> dict[str, scripts.nodes.Expression | None]:
    if isinstance(node, scripts.nodes.Identifier):
        if node.name in definitions:
            definition = definitions.pop(node.name)
            return {
                **used_definitions(definitions, definition),
                **{node.name: definition},
            }
        return {node.name: None}
    result: dict[str, scripts.nodes.Expression | None] = {}
    for child in reversed(node.children):
        for name, defn in used_definitions(definitions, child).items():
            if name not in result or defn is not None:
                result[name] = defn
    return result


def get_expression_dependencies(
    script: scripts.nodes.Script,
    node: scripts.nodes.Expression,
    name: str,
    builtins: Container[str],
) -> tuple[str, frozenset[str]]:
    """
    Get information about the definition of a table.

    :return:
        a tuple consisting of an HTML rendering of the code on which the
        definition of the node depends, and a frozenset of names used in
        defining the node.
    """
    show = used_definitions(
        {
            n.identifier.name: n.expression
            for n in script.statements
            if isinstance(n, scripts.nodes.Assignment)
        },
        node,
    )
    show[name] = node

    return (
        Formatter(builtins, editable=False).format_node(
            scripts.nodes.Script(
                slice(0),
                tuple(
                    scripts.nodes.Assignment(
                        slice(0), scripts.nodes.Identifier(slice(0), n), d
                    )
                    for n, d in show.items()
                    if d is not None
                ),
            )
        ),
        frozenset(show),
    )


def find_definition(
    script: scripts.nodes.Script, name: str
) -> scripts.nodes.Assignment:
    return next(
        n
        for n in script.statements
        if isinstance(n, scripts.nodes.Assignment) and n.identifier.name == name
    )


def get_page_definition(
    script: scripts.nodes.Script, builtins: Container[str], index: int
) -> tuple[slice | None, str, frozenset[str]]:
    """
    Get information about the definition of a table.

    :return:
        a tuple consisting of a slice giving the source code position
        of the definition, an HTML rendering of the code on which the
        definition of the table depends, and a frozenset of names used
        in defining the table.
    """
    definition = find_definition(script, "pages").expression
    definition_name = "pages"
    position = None
    if isinstance(definition, scripts.nodes.Array):
        definition = definition.values[index]
        definition_name = "this page"
        position = definition.pos
    html, depends = get_expression_dependencies(
        script, definition, definition_name, builtins
    )
    return position, html, depends
