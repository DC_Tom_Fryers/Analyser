"""This module provides functions for modifying scripts as strings."""
from __future__ import annotations

from collections.abc import Iterable

from analyser import scripts
from analyser.script_edit import strings

BLANK = '"___"'


def get_action_replacement(
    action: str, pos: slice, new_value: str | None, script: str
) -> tuple[slice, str]:
    new_value = new_value if new_value is not None else BLANK
    if action == "edit":
        content = script[pos]
        if strings.needs_brackets(script, pos) and strings.in_brackets(content):
            content = content[1:-1]
        content = scripts.parse.escape(content)
        return (pos, f'"___{content}"')
    if action == "edit_identifier":
        return (pos, f"___{script[pos]}")
    if action == "add_statement_above":
        return (slice(pos.start, pos.start), f"___: {BLANK}\n")
    if action == "add_statement_below":
        replacement = f"___: {BLANK}\n"
        if script[pos.stop - 1] != "\n":
            replacement = "\n" + replacement
        return (slice(pos.stop, pos.stop), replacement)
    if action == "remove_statement":
        return (slice(pos.start, pos.stop), "")
    if action == "duplicate_statement":
        replacement = script[pos]
        if replacement[-1] != "\n":
            replacement += "\n"
        return (slice(pos.start, pos.start), replacement)
    if action == "remove":
        return (pos, "")
    if action == "extend":
        comma_or_ws_pos = strings.find_comma(script, pos.stop - 1, -1)
        replacement = ", " if script[comma_or_ws_pos - 1] != "[" else ""
        replacement += new_value
        return (slice(comma_or_ws_pos, comma_or_ws_pos), replacement)
    if action == "shrink":
        start_pos = strings.find_comma(script, pos.start, -1)
        last_pos = pos.stop - 1
        if script[start_pos] != ",":
            last_pos = strings.find_comma(script, last_pos, 1)
        return (slice(start_pos, last_pos + 1), "")
    if action == "add_array":
        return (pos, strings.bracket(new_value if new_value != BLANK else "", "["))
    if action == "add_tuple":
        return (pos, f"({new_value}, {BLANK})")
    if action == "add_function":
        replacement = f"___ -> {new_value}"
        if strings.needs_brackets(script, pos):
            replacement = strings.bracket(replacement)
        return (pos, replacement)
    if action in {"add_function_call", "map_function"}:
        if not strings.in_brackets(new_value, "([") and " " in new_value:
            new_value = strings.bracket(new_value)
        replacement = f"{BLANK} {new_value}"
        if action == "map_function":
            replacement = f"map {replacement}"
        if strings.needs_brackets(script, pos):
            replacement = strings.bracket(replacement)
        return (pos, replacement)
    if action == "show":
        return (pos, script[pos].replace("[show]", "[hide]"))
    if action == "hide":
        return (pos, script[pos].replace("[hide]", "[show]"))
    raise ValueError(f"unrecognised action: {action}")


def modify_script(script: str, action_list: Iterable[tuple[str, str]]) -> str:
    """Return an edited script based on the list of actions."""
    # This is complicated by ensuring that if the user presses a button
    # next to a text box the contents of the box are wrapped rather than
    # lost.

    # Group actions by their position
    pos_actions: dict[tuple[int, int], dict[str, str]] = {}
    for name, value in action_list:
        action, pos_str = name.split("@")
        start, _, stop = pos_str.partition("-")
        pos_tuple = (int(start), int(stop))
        pos_actions.setdefault(pos_tuple, {})
        pos_actions[pos_tuple][action] = value

    replacements = []
    for pos_tuple, actions in pos_actions.items():
        pos = slice(*pos_tuple)
        new_value = None
        if "new" in actions:
            maybe_new_val = actions.pop("new").strip()
            if maybe_new_val:
                new_value = maybe_new_val
                if not actions:
                    # New value from text box only
                    if strings.needs_brackets(script, pos) and script[pos][0] == '"':
                        new_value = strings.bracket(new_value)
                    replacements.append((pos, new_value))

        if not actions:
            continue
        if len(actions) > 1:
            raise ValueError(f"too many actions: {actions}")
        replacements.append(
            get_action_replacement(actions.popitem()[0], pos, new_value, script)
        )

    return strings.do_replacements(script, replacements)
