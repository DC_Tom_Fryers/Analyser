"""This module provides time parsing functions."""
from __future__ import annotations

import re
from datetime import time

HOUR_24 = r"(?:0\d|1\d|2[0-3])"
HOUR_12 = r"(?:[1-9]|1[0-2])"
MIN_SEC = r"(?:[0-5]\d)"


def make_time_re(pattern: str, /) -> re.Pattern[str]:
    return re.compile(
        pattern.replace("HOUR_24", HOUR_24)
        .replace("HOUR_12", HOUR_12)
        .replace("MIN_SEC", MIN_SEC),
        re.ASCII,
    )


ISO_TIME_RE = make_time_re(r"(?<![\d:])HOUR_24:MIN_SEC(:?:MIN_SEC)?(:?\.\d+)?(?![\d:])")
TIME_12_RE = make_time_re(r"(?<![\d:])HOUR_12:MIN_SEC(?::MIN_SEC)? ?[ap]\.? ?m\.?")


def time_12_format(string: str, /) -> time:
    string = "".join(c for c in string if c in "0123456789:.ap").strip(".")
    try:
        pm_adjustment = {"a": 0, "p": 12}[string[-1]]
        string = string[:-1]
    except KeyError:
        raise ValueError("can't format time") from None

    parts = string.split(":")
    hour = int(parts.pop(0))
    if hour == 12:
        hour = 0
    hour += pm_adjustment
    minutes = int(parts.pop(0))
    if not parts:
        return time(hour, minutes)
    seconds, microseconds = divmod(round(10 ** 6 * float(parts[0])), 10 ** 6)
    return time(hour, minutes, seconds, microseconds)


TIME_FORMATS = ((TIME_12_RE, time_12_format), (ISO_TIME_RE, time.fromisoformat))


def to_time(string: str, /) -> time:
    string = string.casefold()
    for regex, format_func in TIME_FORMATS:
        if date_string := regex.search(string):
            return format_func(date_string.group(0))
    raise ValueError(f"Cannot interpret {string!r} as a time")
