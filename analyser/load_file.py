"""This module can load a dataframe in one of several formats."""
from __future__ import annotations

import string
from collections.abc import Callable
from contextlib import suppress
from dataclasses import dataclass
from functools import partial
from typing import BinaryIO, cast

import pandas as pd

from analyser.analysis import AVAILABLE_NAMES
from analyser.html_gen import tag


@dataclass
class Filetype:
    name: str
    extensions: tuple[str, ...]
    read: Callable[[BinaryIO], pd.DataFrame]
    required_module: str | None = None

    @property
    def description(self) -> str:
        extension_string = ", ".join(
            tag.span(f"*.{e}", class_="mono") for e in self.extensions
        )
        return tag.li(f"{self.name} ({extension_string})")

    @property
    def is_available(self):
        if self.required_module is None:
            return True
        try:
            __import__(self.required_module)
            return True
        except ImportError:
            return False


def try_json(file: BinaryIO, /) -> pd.DataFrame:
    """Try several methods to load a JSON data to a table."""
    text = file.read()
    for orient in ("table", "records", "split", "columns", "index", "split"):
        with suppress(ValueError, TypeError, AssertionError, KeyError):
            return pd.read_json(text, orient=orient, dtype=False)
    raise ValueError("Not valid JSON")


FILETYPES = tuple(
    f
    for f in (
        Filetype("Parquet", ("parquet",), pd.read_parquet, "pyarrow"),
        Filetype("Comma-seperated", ("csv",), partial(pd.read_csv, dtype="string")),
        Filetype(
            "Tab-seperated", ("tsv", "tab"), partial(pd.read_table, dtype="string")
        ),
        Filetype(
            "OpenDocument Spreadsheet",
            ("ods",),
            partial(pd.read_excel, engine="odf", dtype="string"),
            "odfpy",
        ),
        Filetype(
            "Legacy Excel worksheet",
            ("xls",),
            partial(pd.read_excel, dtype="string"),
            "xlrd",
        ),
        Filetype(
            "Excel workbook",
            ("xlsx", "xlsm"),
            partial(pd.read_excel, dtype="string"),
            "openpyxl",
        ),
        Filetype(
            "Excel binary worksheet",
            ("xlsb",),
            partial(pd.read_excel, engine="pyxlsb", dtype="string"),
            "pyxlsb",
        ),
        Filetype("JSON", ("json",), try_json),
        Filetype("XML", ("xml",), pd.read_xml, "lxml"),
        Filetype(
            "HTML",
            ("html", "htm"),
            lambda x: sorted(pd.read_html(x), key=len)[-1],
            "lxml",
        ),
    )
    if f.is_available
)

HEADING_CHARS = string.ascii_letters + string.digits + "_"


def load(data: BinaryIO, filename: str) -> pd.DataFrame:
    """Load a file to a dataframe, recognising the filename extension."""
    extension = filename.split(".")[-1].casefold()
    for filetype in FILETYPES:
        if extension in filetype.extensions:
            df = filetype.read(data)
            break
    else:
        raise ValueError("bad file type")

    if set(df.dtypes) != {pd.StringDtype()}:
        df = cast(pd.DataFrame, df.astype("string", copy=False))
    new_names: dict[str, str] = {}
    for column in df.columns:
        new_name = "".join(c for c in column if c in HEADING_CHARS)
        if new_name[0] in string.digits or new_name in AVAILABLE_NAMES:
            new_name += "_column"
        while new_name in new_names.values():
            new_name += "_"
        new_names[column] = new_name
    df.rename(columns=new_names, inplace=True)
    return df.fillna("")
