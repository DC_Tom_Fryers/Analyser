"""This module provides functions for recognising column types."""
from __future__ import annotations

import re
import string
from collections.abc import Callable, Container, Iterator
from contextlib import suppress
from dataclasses import dataclass
from math import erfc, log, sqrt
from statistics import fmean as mean

import numpy as np
import pandas as pd
from scipy import optimize

from analyser import dates, times
from analyser.script_edit import strings


def has_word_re(word: str) -> re.Pattern[str]:
    """
    Create a regular expression that matches an entire word.

    The word must either be surrounded by non-letter characters or be
    part of a camelCase phrase.
    """
    return re.compile(
        "|".join(
            (
                r"(?i:(?<![a-z])WORD(?![a-z]))".replace("WORD", word),
                r"(?<![A-Z])WORD(?![a-z])".replace("WORD", word.capitalize()),
            )
        )
    )


SUPPLIER_RES = [
    (x, has_word_re(x))
    for x in ("ltd", "limited", "plc", "llc", "inc", "holdings", "co")
]

NUMBER_RE = re.compile(r"(?<!\d)\d+(?!\d)")

# Money is often written with two places after the decimal point, so
# these match only expressions like that. MONEY_RE_ZEROES looks for
# trailing zeroes, because they are strongly indicative that the two
# decimal places are deliberate.
MONEY_RE = re.compile(r"[\d,\w]+\.\d{2}(?!\d)")
MONEY_RE_ZEROES = re.compile(r"\d+\.\d0(?!\d)")
CURRENCIES = {
    ("USD", "$"),
    ("EUR", "€"),
    ("JPY", "¥"),
    ("GBP", "£"),
    ("AUD", "$"),
    ("CAD", "$"),
}


def like_full_match(regex: re.Pattern[str], string: str) -> float:
    """Get ⅓ points for containing, but 1 point for a complete match."""
    if regex.fullmatch(string):
        return 1
    return bool(regex.search(string)) / 3


# TODO Use this on the whole sample, rather than punishing individual
# entries. A Mann-Whitney U test might be best, but will make the models
# more complicated, and none of this module is very precise anyway.
@dataclass
class Model:
    """A normal distribution model."""

    mu: float
    sigma: float

    def prob_of(self, value: float, /) -> float:
        """Get the two-tailed probability of value."""
        return erfc(abs(value - self.mu) / (self.sigma * sqrt(2))) * 2


supplier_model = Model(19.2, 8.1)
invoice_num_model = Model(10.1, 6.4)


def character_proportion(string: str, characters: Container[str]) -> float:
    if not string:
        return 0.5
    return mean(c in characters for c in string)


def numbers(string_: str, /) -> Iterator[str]:
    """Iterate over all the numbers in the string."""
    for m in NUMBER_RE.finditer(string_):
        yield m.group(0)


def weird_letters(value: str, /) -> float:
    """
    Check if unusual letters appear often in the string.

    For normal English this will be about zero, but for random
    characters, as might be expected in an ID number, the value will
    average around 1.
    """
    letters = [i for i in value.casefold() if i in string.ascii_lowercase]
    if not letters:
        return 0
    return (
        2 * (len(letters) - sum(i in "etaoinshr" for i in letters) / 0.7) / len(letters)
    )


def id_number_score(value: str, /) -> float:
    if not value:
        return 0
    return (
        invoice_num_model.prob_of(len(value))
        + 3 * (value.count(".") != 1)
        + sum(
            log(max(int(n) / 10000, 1)) if not set(n).issubset("0") else 3
            for n in numbers(value)
        )
        + weird_letters(value)
        + ("/" in value)
        + 2 * ("-" in value)
    )


def company_score(value: str, /) -> float:
    """
    Try to determine if a string represents a company.

    Particularly look for common company name things ("co", "plc" etc.).
    """
    if not value:
        return 0
    return (
        supplier_model.prob_of(len(value))
        + 5 * sum(4 if i[1].search(value) else 1 for i in SUPPLIER_RES if i[0] in value)
        + character_proportion(value, string.ascii_letters + " ()")
        + (" " in value)
    )


def money_score(value: str, /) -> float:
    """
    Try to determine if a string represents money.

    This looks for common currency symbols at the start (£2.10) or
    names at the end (210.00 GBP). It also checks for common currency
    formatting and dislikes letters.
    """
    if not value:
        return 0
    base_score = 0
    for c in CURRENCIES:
        if value[0] == c[1]:
            value = value[1:]
            base_score += 1
            break
        if value.endswith(c[0]):
            value = strings.removesuffix(value, c[0])
            base_score += 1
            break
    if not value:
        return -2
    value = value.replace(" ", "").replace(",", "")
    with suppress(ValueError):
        base_score += 3 * (abs(float(value)) < 100000000)
    return (
        base_score
        + ("." in value)
        + 4 * like_full_match(MONEY_RE, value)
        + 6 * like_full_match(MONEY_RE_ZEROES, value)
        - 2 * character_proportion(value, string.ascii_letters)
    )


def currency_score(value: str, /) -> float:
    return 5 * any(value.upper() in c for c in CURRENCIES)


def date_score(value: str, /) -> float:
    return max(
        like_full_match(r, value)
        for r in (dates.ISO_DATE_RE, dates.UK_DATE_RE, dates.UK_WRITTEN_DATE_RE)
    )


def time_score(value: str, /) -> float:
    return max(like_full_match(r, value) for r in (times.ISO_TIME_RE, times.TIME_12_RE))


def english_text_score(value: str, /) -> float:
    return 2 * character_proportion(value, string.ascii_letters + " ") - weird_letters(
        value
    )


def match_columns(
    data: pd.DataFrame,
    column_scores: dict[str, tuple[Callable[[str], float], tuple[str, ...]]],
) -> dict[str, str]:
    # Calculate the values of matching each column to each meaning.
    scores = np.empty((len(column_scores), len(data.columns)))
    for column, (function, keywords) in enumerate(column_scores.values()):
        scores[column] = data.sample(min(2000, len(data))).applymap(function).mean()
        for index, data_column in enumerate(data.columns):
            scores[column, index] += sum(i in data_column.casefold() for i in keywords)
    mapping = optimize.linear_sum_assignment(scores, maximize=True)[1]
    return dict(zip(column_scores, (data.columns[c] for c in mapping)))
