from __future__ import annotations

from collections import defaultdict
from collections.abc import Hashable, Iterable, Mapping
from contextlib import suppress
from dataclasses import dataclass, field
from typing import Any, cast

from analyser.scripts.errors import ScriptTypeError


@dataclass(frozen=True, init=False)
class Type:
    __slots__ = ("variables",)
    variables: Iterable[Var]

    def restricted(self, other: Type, classes: dict[str, Typeclass]) -> dict[Var, Type]:
        """
        Get type variables which must change to make self compatible with other.

        :raises ScriptTypeError: The types are incompatible.
        """

    def substitute(self, sub: Mapping[Var, Type], /) -> Type:
        """Get a new type with the given variables substituted."""


typevar_count = 0


@dataclass(frozen=True, repr=False, init=False)
class Var(Type):
    __slots__ = ("classes", "id")
    classes: frozenset[str] | None
    id: int
    variables: tuple[Var]

    def __init__(self, classes: frozenset[str] | None = None, id_: int | None = None):
        if id_ is None:
            global typevar_count
            typevar_count += 1
        object.__setattr__(self, "classes", classes)
        object.__setattr__(self, "id", typevar_count)
        object.__setattr__(self, "variables", (self,))

    def restricted(self, other: Type, classes: dict[str, Typeclass]) -> dict[Var, Type]:
        if other == self:
            return {}
        if self.classes is None:
            return {self: other}
        if isinstance(other, Var):
            if other.classes is None:
                return {other: self}
            new = Var(self.classes | other.classes)
            return {self: new, other: new}

        possible_implementations = [other]
        for class_ in self.classes:
            new_possible_implementations = []
            for impl_class in classes[class_].implementations:
                for old in possible_implementations:
                    with suppress(ScriptTypeError):
                        new_possible_implementations.append(
                            old.substitute(old.restricted(impl_class, classes))
                        )
            possible_implementations = new_possible_implementations
        if not possible_implementations:
            raise ScriptTypeError(
                f"{other} doesn't implement {' '.join(self.classes)}."
            )
        return {self: other}

    def substitute(self, sub: Mapping[Var, Type], /) -> Type:
        had = set()
        result: Type = self
        while result in sub:
            had.add(result)
            result = sub[cast(Var, result)]
            if result in had:
                raise TypeError(f"circular type substitution: {sub}")
        return result

    def copy(self) -> Var:
        return Var(self.classes)

    def __hash__(self) -> int:
        return hash((self.classes, self.id))

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Var) and self.id == other.id

    def __str__(self) -> str:
        classes = self.classes if self.classes is not None else ("Any",)
        return f"{' '.join(classes)} {self.id}"

    def __repr__(self) -> str:
        return f"Var({self.classes!r}, {self.id})"


@dataclass(frozen=True, init=False, repr=False)
class Primitive(Type):
    __slots__ = ("name",)
    name: str
    variables: tuple[()]

    def __init__(self, name: str) -> None:
        object.__setattr__(self, "name", name)
        object.__setattr__(self, "variables", ())

    def substitute(self, sub: Mapping[Var, Type], /) -> Primitive:
        return self

    def restricted(self, other: Type, classes: dict[str, Typeclass]) -> dict[Var, Type]:
        if other == self:
            return {}
        if isinstance(other, Var):
            if other.classes is not None:
                for class_ in other.classes:
                    if self not in classes[class_].implementations:
                        raise ScriptTypeError(f"{self} does not implement {class_}.")
            return {other: self}
        raise ScriptTypeError(f"{self} is incompatible with {other}.")

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}('{self.name}')"


@dataclass(frozen=True, init=False, repr=False)
class Generic(Type):
    __slots__ = ("types",)
    types: tuple[Type, ...]
    variables: frozenset[Var]

    def __init__(self, *types: Type) -> None:
        object.__setattr__(self, "types", types)
        if not types:
            object.__setattr__(self, "variables", frozenset())
            return
        variables = types[0].variables
        if len(types) > 1:
            variables = set(variables)
            for type_ in types[1:]:
                variables.update(type_.variables)
        object.__setattr__(self, "variables", frozenset(variables))

    def substitute(self, sub: Mapping[Var, Type], /) -> Generic:
        if self.variables.isdisjoint(sub):
            return self
        return type(self)(*(v.substitute(sub) for v in self.types))

    def restricted(self, other: Type, classes: dict[str, Typeclass]) -> dict[Var, Type]:
        new_types: dict[Var, Type] = {}
        if isinstance(other, GenericVar):
            new_types[other.types[0]] = type(self)(
                *(Var() for _ in range(len(self.types)))
            )
            other = type(self)(*other.types[1:])
        if isinstance(other, Var):
            # REVIEW Might need to ban recursive types
            return {other: self}
        if type(self) is not type(other):
            raise ScriptTypeError(f"{self} is incompatible with {other}.")
        other = cast(Generic, other)
        if len(self.types) != len(other.types):
            raise ScriptTypeError(
                "Wrong number of type parameters"
                f" ({len(self.types)} /= {len(other.types)})."
            )
        for own_type, other_type in zip(self.types, other.types):
            # REVIEW I think these two lines should be needed, but they
            # don't seem to do anything
            own_type = own_type.substitute(new_types)
            other_type = other_type.substitute(new_types)
            new_types.update(own_type.restricted(other_type, classes))
        return new_types

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__qualname__}({', '.join(repr(t) for t in self.types)})"
        )


@dataclass(frozen=True, init=False, repr=False)
class GenericVar(Generic):
    __slots__ = ()
    types: tuple[Any, ...]  # tuple[Var, Type, ...] is unsupported

    def substitute(self, sub: Mapping[Var, Type], /) -> Generic:
        if self.types[0] in sub:
            new_type = sub[self.types[0]]
            if isinstance(new_type, Generic):
                new_type = new_type.substitute(
                    dict(zip(cast("tuple[Var]", new_type.types), self.types[1:]))
                )
                return new_type.substitute(sub)
        return super().substitute(sub)

    def restricted(self, other: Type, classes: dict[str, Typeclass]) -> dict[Var, Type]:
        if isinstance(other, Generic) and not isinstance(other, GenericVar):
            return other.restricted(self, classes)

        return super().restricted(other, classes)

    def __str__(self) -> str:
        return f"({str(self.types[0])}) ({', '.join(str(t) for t in self.types[1:])})"


@dataclass(frozen=True, init=False, repr=False)
class Array(Generic):
    __slots__ = ()
    types: tuple[Type]

    def __str__(self) -> str:
        return f"[{self.types[0]}]"


@dataclass(frozen=True, init=False, repr=False)
class Tuple(Generic):
    __slots__ = ()
    types: tuple[Type, Type]

    def __str__(self) -> str:
        part: Type = self
        types = []
        while isinstance(part, Tuple):
            types.append(part.types[0])
            part = part.types[1]
        types.append(part)

        return f"({', '.join(str(t) for t in types)})"


@dataclass(frozen=True, init=False, repr=False)
class Option(Generic):
    __slots__ = ()
    types: tuple[Type]

    def __str__(self) -> str:
        type_name = str(self.types[0])
        if isinstance(self.types[0], Function):
            type_name = f"({type_name})"
        return f"{type_name}?"


@dataclass(frozen=True, init=False, repr=False)
class Function(Generic):
    __slots__ = ()
    types: tuple[Type, Type]

    @property
    def parameter_type(self) -> Type:
        return self.types[0]

    @property
    def return_type(self) -> Type:
        return self.types[1]

    def __str__(self) -> str:
        parameter_type_name = str(self.parameter_type)
        # -> is right-associative so the parameter type must be
        # parenthesised if it is a function
        if isinstance(self.parameter_type, Function):
            parameter_type_name = f"({parameter_type_name})"
        return f"{parameter_type_name} -> {self.return_type}"

    @classmethod
    def new(cls, param_types: tuple[Type, ...], return_type: Type) -> Function:
        """
        Curry a function type.

        Convert a normal function type (A, ..., Z) -> A' to a curried
        fuction type A -> ... -> Z -> A'.
        """
        if len(param_types) > 1:
            return_type = cls.new(param_types[1:], return_type)

        return cls(param_types[0], return_type)


integer = Primitive("Integer")
boolean = Primitive("Boolean")
float_ = Primitive("Float")
string = Primitive("String")


@dataclass(frozen=True)
class Typeclass:
    __slots__ = ("requires", "implementations")
    requires: frozenset[str]
    implementations: dict[Type, dict[str, Variable]]


@dataclass(frozen=True)
class Variable:
    __slots__ = ("type_", "value")
    type_: Type
    value: Any


@dataclass
class _TypeSlots:
    slots: dict[frozenset[str] | None, defaultdict[Hashable, Var]] = field(
        default_factory=dict
    )

    def __getitem__(self, value: Hashable) -> Var:
        self.slots.setdefault(None, defaultdict(Var))
        return self.slots[None][value]

    def bound(self, *classes: str) -> defaultdict[Hashable, Var]:
        classes_set = frozenset(classes)
        self.slots.setdefault(classes_set, defaultdict(lambda: Var(classes_set)))
        return self.slots[classes_set]


T = _TypeSlots()
