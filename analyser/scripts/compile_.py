from __future__ import annotations

import graphlib
import keyword
from collections import defaultdict
from collections.abc import Iterable, Iterator
from contextlib import suppress
from dataclasses import dataclass
from functools import reduce
from typing import Any

from analyser.scripts import functions, infer, nodes, types
from analyser.scripts.errors import ScriptNameError, ScriptTypeError
from analyser.scripts.types import Type, Typeclass


def bracket(string: str, /) -> str:
    return f"({string})"


SPECIAL_PREFIXES = set("mpr")
FAST_CONSTANTS = {"false": "False", "true": "True", "none": "None"}
FAST_FUNCTIONS = {
    "=": "({0}=={1})",
    "/=": "({0}!={1})",
    "<": "({0}<{1})",
    ">": "({0}>{1})",
    "<=": "({0}<={1})",
    ">=": "({0}>={1})",
    "contains": "({0} in {1})",
    "in": "({0} in {1})",
    "some": "({0},)",
    "and": "({0} and {1})",
    "or": "({0} or {1})",
    "not": "(not {0})",
    "tail": "{0}[1:]",
    "id": "{0}",
    "take": "{1}[:{0}]",
    "drop": "{1}[{0}:]",
    "case_fold": "{0}.casefold()",
    "join": "{0}.join({1})",
}
FAST_METHODS = {
    "=": "__eq__",
    "/=": "__ne__",
    "<": "__lt__",
    ">": "__gt__",
    "<=": "__le__",
    ">=": "__ge__",
    "join": "join",
}


@dataclass
class Compiler:
    __slots__ = (
        "inferred",
        "variables",
        "classes",
        "typeclass_option",
        "required_types",
        "available_raw",
    )
    inferred: infer.Inferrer
    variables: dict[str, Any]
    classes: dict[str, Typeclass]

    def get_implementation(self, expr: nodes.Identifier) -> tuple[Typeclass, Type, str]:
        type_ = self.inferred.nodes[id(expr)].substitute(self.typeclass_option)
        class_ = self.classes[self.variables[expr.name].class_]
        for name, impl in class_.implementations.items():
            with suppress(ScriptTypeError):
                type_.restricted(impl[expr.name].type_, self.classes)
                return class_, name, expr.name
        raise TypeError("no implementation found")

    def polymorphic_use_name(self, node: nodes.Identifier, /) -> str:
        return self.polymorphic_name(
            node.name, self.inferred.nodes[id(node)].substitute(self.typeclass_option)
        )

    def polymorphic_name(self, name: str, type_: types.Type) -> str:
        return "p" + self.encode_name(f"{name} {type_}")

    def method_name(self, name: str, type_: types.Type) -> str:
        return "m" + self.encode_name(f"{name}:{type_}")

    def compile_expr(self, expr: nodes.Expression) -> str:
        if isinstance(expr, nodes.FunctionCall):
            return self.compile_function_call(expr)
        if isinstance(expr, nodes.Identifier):
            return self.compile_identifier(expr)
        if isinstance(expr, nodes.Literal):
            return repr(expr.value)
        if isinstance(expr, nodes.Array):
            return bracket(
                ",".join(self.compile_expr(e) for e in expr.values)
                + ("," if len(expr.values) == 1 else "")
            )
        if isinstance(expr, nodes.Tuple):
            result = ["("]
            for sub in expr.values:
                result.append(self.compile_expr(sub))
                result.append(",(")
            result.pop()
            result.append(")" * (len(expr.values)))
            return "".join(result)
        if isinstance(expr, nodes.Function):
            param = self.encode_name(expr.parameter.name)
            body = self.compile_expr(expr.body)
            return bracket(f"lambda {param},/:{body}")
        if isinstance(expr, nodes.AttributeAccess):
            return (
                self.compile_expr(expr.value)
                + "[1]" * expr.attribute.value
                + f"[{expr.part}]"
            )
        if isinstance(expr, nodes.IfThenElse):
            return bracket(
                " ".join(
                    (
                        self.compile_expr(expr.true_val),
                        "if",
                        self.compile_expr(expr.cond),
                        "else",
                        self.compile_expr(expr.false_val),
                    )
                )
            )
        raise NotImplementedError(f"unable to compile {expr}")

    def compile_function_call(self, expr: nodes.FunctionCall) -> str:
        chain = []
        head: nodes.Expression = expr
        while isinstance(head, nodes.FunctionCall):
            chain.append(head)
            head = head.function
        if isinstance(head, nodes.Identifier):
            name = head.name
            if name in self.variables or name in self.available_raw:
                function = self.variables.get(name)
                if isinstance(function, functions.Method):
                    class_, type_, encoded_name = self.get_implementation(head)
                    function = class_.implementations[type_][encoded_name].value
                    encoded_name = self.method_name(name, type_)
                elif (
                    name in self.inferred.polymorphic_uses
                    and name in self.available_raw
                ):
                    encoded_name = self.polymorphic_use_name(head)
                else:
                    encoded_name = self.encode_name(name)

                arguments = (
                    function.raw_params
                    if name in self.variables
                    else self.available_raw[name]
                )
                if arguments == len(chain):
                    args = [self.compile_expr(n.argument) for n in reversed(chain)]
                    try:
                        return FAST_FUNCTIONS[name].format(*args)
                    except KeyError:
                        return f"r{encoded_name}({','.join(args)})"
                if arguments == 2 and len(chain) == 1 and name in FAST_METHODS:
                    return (
                        f"({self.compile_expr(chain[0].argument)}).{FAST_METHODS[name]}"
                    )
        function = self.compile_expr(expr.function)
        argument = self.compile_expr(expr.argument)
        return f"{function}({argument})"

    def compile_identifier(self, expr: nodes.Identifier) -> str:
        with suppress(KeyError):
            return FAST_CONSTANTS[expr.name]
        if expr.name in self.variables:
            if isinstance(self.variables[expr.name], functions.Method):
                _, type_, name = self.get_implementation(expr)
                return self.method_name(name, type_)
        elif expr.name in self.inferred.polymorphic_uses:
            return self.polymorphic_use_name(expr)
        return self.encode_name(expr.name)

    @staticmethod
    def encode_name(name: str) -> str:
        """
        Encode an arbitrary string to a valid Python identifier.

        Underscores and invalid characters are replaced with their
        Unicode value in hexadecimal, surrounded by underscores.
        Keywords, empty strings or names with a reserved special prefix
        are prefixed with an underscore.
        """
        name = "".join(
            c if c != "_" and f"a{c}".isidentifier() else f"_{ord(c):x}_" for c in name
        )
        if not name or keyword.iskeyword(name) or name[0] in SPECIAL_PREFIXES:
            name = "_" + name
        return name

    @staticmethod
    def decode_name(name: str) -> str:
        """Undo the encoding done by `encode_name`."""
        if name.count("_") % 2:
            name = name[1:]
        done = []
        while True:
            try:
                start = name.index("_")
            except ValueError:
                break
            end = name.index("_", start + 1)
            done.append(name[:start])
            done.append(chr(int(name[start + 1 : end], 16)))
            name = name[end + 1 :]
        return "".join(done) + name

    @staticmethod
    def sort_statements(
        statements: Iterable[nodes.Statement],
    ) -> list[nodes.Assignment]:
        """Ensure dependencies of a function precede it in definition."""
        assignments = [s for s in statements if isinstance(s, nodes.Assignment)]
        names = {s.identifier.name for s in assignments}

        def depends(node: nodes.Expression) -> set[str]:
            if isinstance(node, nodes.Function):
                return set()
            if isinstance(node, nodes.Identifier):
                return {node.name} if node.name in names else set()
            return reduce(set.union, (depends(c) for c in node.children), set())

        try:
            order = tuple(
                graphlib.TopologicalSorter(
                    {s.identifier.name: depends(s.expression) for s in assignments}
                ).static_order()
            )
        except graphlib.CycleError as error:
            raise ScriptNameError(
                f"Circular definitions of {', '.join(error.args[1][:-1])}."
            ) from None
        return sorted(assignments, key=lambda s: order.index(s.identifier.name))

    def add_required_types(self, name: str, type_: Type) -> None:
        """
        Find the required implementations for a polymorphic function.

        Add to self.required_types the necessary types for the function
        called name. Also find types for any dependencies of this
        function.
        """
        if type_.variables:
            these_required_types = set()
            for use in self.inferred.polymorphic_uses[name]:
                used_as_type = self.inferred.nodes[id(use)]
                if not used_as_type.variables:
                    these_required_types.add(used_as_type)
        else:
            these_required_types = {type_}

        new_required_types = these_required_types.difference(self.required_types[name])
        self.required_types[name] |= these_required_types
        for req_type in new_required_types:
            restriction = self.inferred.variables[name].restricted(
                req_type, self.classes
            )
            for dep in self.inferred.polymorphic_depends[name]:
                self.add_required_types(
                    dep.name, self.inferred.nodes[id(dep)].substitute(restriction)
                )

    def compile_(self, script: nodes.Script) -> str:
        """Compile a script node to a Python script."""
        self.required_types: defaultdict[str, set[Type]] = defaultdict(set)
        assignments = self.sort_statements(script.statements)
        self.available_raw = {}
        for assignment in assignments:
            arity = 0
            expr = assignment.expression
            while isinstance(expr, nodes.Function):
                arity += 1
                expr = expr.body
            if arity > 1:
                self.available_raw[assignment.identifier.name] = arity
        for assignment in assignments:
            type_ = self.inferred.nodes[id(assignment.expression)]
            self.add_required_types(assignment.identifier.name, type_)
        result = []
        for assignment in assignments:
            result += list(self.compile_assignment(assignment))
        return "\n".join(result)

    def compile_assignment(self, statement: nodes.Assignment) -> Iterator[str]:
        """
        Compile an variable assignment to Python code.

        :return: an iterator of lines of Python code
        """
        identifier = statement.identifier.name
        type_ = self.inferred.nodes[id(statement.expression)]
        typeclass_options = tuple(
            type_.restricted(t, self.classes) for t in self.required_types[identifier]
        )
        for self.typeclass_option in typeclass_options:
            this_identifier = identifier
            if self.typeclass_option:
                this_type = type_.substitute(self.typeclass_option)
                if this_type.variables:
                    raise TypeError("cannot compile function of abstract type")
                this_identifier = self.polymorphic_name(this_identifier, this_type)
            else:
                this_identifier = self.encode_name(this_identifier)
            expression = statement.expression
            if isinstance(expression, nodes.Function):
                parameter = self.encode_name(expression.parameter.name)
                yield (
                    f"def {this_identifier}({parameter},/):"
                    f"return {self.compile_expr(expression.body)}"
                )
                if statement.identifier.name in self.available_raw:
                    parameters = []
                    expr: nodes.Expression = expression
                    while isinstance(expr, nodes.Function):
                        parameters.append(self.encode_name(expr.parameter.name))
                        expr = expr.body
                    yield (
                        f"def r{this_identifier}({','.join(parameters)},/):"
                        f"return {self.compile_expr(expr)}"
                    )

            else:
                yield f"{this_identifier}={self.compile_expr(expression)}"
