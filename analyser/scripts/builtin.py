"""This module defines the built-in functions and typeclasses."""
from __future__ import annotations

import decimal
import math
import operator
from functools import reduce

from analyser.scripts import parse, types
from analyser.scripts.functions import Function, Method
from analyser.scripts.types import T, Typeclass, Variable


def mod(a: int, b: int, /) -> tuple[int] | None:
    return None if b == 0 else (a % b,)


def ieee_divide(n: float, d: float, /) -> float:
    try:
        return n / d
    except ZeroDivisionError:
        return n * math.copysign(math.inf, d)


def ieee_sqrt(x: float, /) -> float:
    # Ensure sqrt(-0.0) = -0.0
    if x == 0.0:
        return x
    try:
        return math.sqrt(x)
    except ValueError:
        return math.nan


def ieee_exp(x: float, /) -> float:
    try:
        return math.exp(x)
    except OverflowError:
        return math.inf


def int_to_float(x: int, /) -> float:
    try:
        return float(x)
    except OverflowError:
        if x > 0:
            return math.inf
        return -math.inf


def parse_integer(string: str, /) -> tuple[int] | None:
    if parse.INTEGER_RE.fullmatch(string):
        return (int(string),)
    return None


def parse_float(string: str, /) -> tuple[float] | None:
    if parse.FLOAT_RE.fullmatch(string) or parse.INTEGER_RE.fullmatch(string):
        return (float(string),)
    return None


def string_float(float_: float, /) -> str:
    with decimal.localcontext() as ctx:
        ctx.prec = 20
        value = f"{decimal.Decimal(repr(float_)):f}"
        return value if "." in value else f"{value}.0"


EQ = T.bound("Equatable")[0]
CN = T.bound("Container")[0]
OR = T.bound("Ordered")[0]
RE = T.bound("Representable")[0]
NO = T.bound("Number")[0]
SQ = T.bound("Sequence")[0]

#: A dictionary of built-in classes.
#:
#: :meta hide-value:
CLASSES: dict[str, Typeclass] = {
    "Equatable": Typeclass(
        frozenset(),
        {
            t: {"=": Function.new((t, t), types.boolean, operator.eq)}
            for t in [
                types.boolean,
                types.integer,
                types.float_,
                types.string,
                types.Array(EQ),
                types.Option(EQ),
                types.Tuple(EQ, T.bound("Equatable")[1]),
            ]
        },
    ),
    "Ordered": Typeclass(
        frozenset(("Equatable",)),
        {
            t: {
                "<": Function.new((t, t), types.boolean, operator.lt),
                ">": Function.new((t, t), types.boolean, operator.gt),
                "<=": Function.new((t, t), types.boolean, operator.le),
                ">=": Function.new((t, t), types.boolean, operator.ge),
            }
            for t in (types.integer, types.float_, types.string)
        },
    ),
    "Number": Typeclass(
        frozenset(("Equatable", "Ordered")),
        {
            t: {
                "+": Function.new((t, t), t, operator.add),
                "-": Function.new((t, t), t, operator.sub),
                "*": Function.new((t, t), t, operator.mul),
            }
            for t in (types.integer, types.float_)
        },
    ),
    "Container": Typeclass(
        frozenset(),
        {
            types.Array(T[0]): {
                "map": Function.new(
                    (types.Function(T[0], T[1]), types.Array(T[0])),
                    types.Array(T[1]),
                    lambda f, c, /: tuple(map(f, c)),
                )
            },
            types.Option(T[0]): {
                "map": Function.new(
                    (types.Function(T[0], T[1]), types.Option(T[0])),
                    types.Option(T[1]),
                    lambda f, c, /: (f(c[0]),) if c else None,
                )
            },
        },
    ),
    "Sequence": Typeclass(
        frozenset(),
        {
            t: {
                "&": Function.new((t, t), t, operator.add),
                "length": Function.new((t,), types.integer, len),
                "reverse": Function.new((t,), t, lambda a, /: a[::-1]),
            }
            for t in (types.Array(T[0]), types.string)
        },
    ),
    "Representable": Typeclass(
        frozenset(),
        {
            types.boolean: {
                "string": Function.new(
                    (types.boolean,), types.string, ("false", "true").__getitem__
                ),
                "parse": Function.new(
                    (types.string,),
                    types.Option(types.boolean),
                    {"false": (False,), "true": (True,)}.get,
                ),
            },
            types.integer: {
                "string": Function.new((types.integer,), types.string, str),
                "parse": Function.new(
                    (types.string,), types.Option(types.integer), parse_integer
                ),
            },
            types.float_: {
                "string": Function.new((types.float_,), types.string, string_float),
                "parse": Function.new(
                    (types.string,), types.Option(types.float_), parse_float
                ),
            },
        },
    ),
}

#: A dictionary of built-in values and functions.
#:
#: :meta hide-value:
BUILTINS: dict[str, Variable] = {
    "=": Variable(types.Function.new((EQ, EQ), types.boolean), Method("Equatable")),
    "<": Variable(types.Function.new((OR, OR), types.boolean), Method("Ordered")),
    ">": Variable(types.Function.new((OR, OR), types.boolean), Method("Ordered")),
    "<=": Variable(types.Function.new((OR, OR), types.boolean), Method("Ordered")),
    ">=": Variable(types.Function.new((OR, OR), types.boolean), Method("Ordered")),
    "+": Variable(types.Function.new((NO, NO), NO), Method("Number")),
    "-": Variable(types.Function.new((NO, NO), NO), Method("Number")),
    "*": Variable(types.Function.new((NO, NO), NO), Method("Number")),
    "map": Variable(
        types.Function.new(
            (types.Function(T[0], T[1]), types.GenericVar(CN, T[0])),
            types.GenericVar(CN, T[1]),
        ),
        Method("Container"),
    ),
    "&": Variable(types.Function.new((SQ, SQ), SQ), Method("Sequence")),
    "length": Variable(types.Function.new((SQ,), types.integer), Method("Sequence")),
    "reverse": Variable(types.Function.new((SQ,), SQ), Method("Sequence")),
    "string": Variable(
        types.Function.new((RE,), types.string), Method("Representable")
    ),
    "parse": Variable(
        types.Function.new((types.string,), types.Option(RE)), Method("Representable")
    ),
    "mod": Function.new(
        (types.integer, types.integer), types.Option(types.integer), mod
    ),
    "/": Function.new((types.float_, types.float_), types.float_, ieee_divide),
    "sqrt": Function.new((types.float_,), types.float_, ieee_sqrt),
    "exp": Function.new((types.float_,), types.float_, ieee_exp),
    "float": Function.new((types.integer,), types.float_, int_to_float),
    "range": Function.new(
        (types.integer,), types.Array(types.integer), lambda n, /: tuple(range(n))
    ),
    "case_fold": Function.new((types.string,), types.string, str.casefold),
    "contains": Function.new(
        (types.string, types.string), types.boolean, lambda n, h, /: n in h
    ),
    "join": Function.new(
        (types.string, types.Array(types.string)), types.string, str.join
    ),
    "id": Function.new((T[0],), T[0], lambda x, /: x),
    "?": Function.new(
        (T[0], types.Option(T[0])), T[0], lambda d, v, /: v[0] if v else d
    ),
    "some": Function.new((T[0],), types.Option(T[0]), lambda value, /: (value,)),
    "none": Variable(types.Option(T[0]), None),
    "true": Variable(types.boolean, True),
    "false": Variable(types.boolean, False),
    "not": Function.new((types.boolean,), types.boolean, operator.not_),
    "and": Function.new(
        (types.boolean, types.boolean), types.boolean, lambda x, y, /: x and y
    ),
    "or": Function.new(
        (types.boolean, types.boolean), types.boolean, lambda x, y, /: x or y
    ),
    "all": Function.new((types.Array(types.boolean),), types.boolean, all),
    "any": Function.new((types.Array(types.boolean),), types.boolean, any),
    "index": Function.new(
        (types.Array(T[0]), types.integer),
        types.Option(T[0]),
        lambda a, i, /: a[i : i + 1] if 0 <= i < len(a) else None,
    ),
    "head": Function.new(
        (types.Array(T[0]),), types.Option(T[0]), lambda x, /: x[:1] if x else None
    ),
    "tail": Function.new((types.Array(T[0]),), types.Array(T[0]), lambda x, /: x[1:]),
    "take": Function.new(
        (types.integer, types.Array(T[0])), types.Array(T[0]), lambda n, a, /: a[:n]
    ),
    "drop": Function.new(
        (types.integer, types.Array(T[0])), types.Array(T[0]), lambda n, a, /: a[n:]
    ),
    "in": Function.new(
        (EQ, types.Array(EQ)), types.boolean, lambda x, a, /: operator.contains(a, x)
    ),
    "reduce": Function.new(
        (types.Function.new((T[0], T[0]), T[0]), types.Array(T[0])),
        types.Option(T[0]),
        lambda f, s, /: (reduce(lambda x, y, /: f(x)(y), s),) if s else None,
    ),
    "fold": Function.new(
        (types.Function.new((T[0], T[1]), T[0]), T[0], types.Array(T[1])),
        T[0],
        lambda f, i, s, /: reduce(lambda x, y, /: f(x)(y), s, i),
    ),
    "filter": Function.new(
        (types.Function(T[0], types.boolean), types.Array(T[0])),
        types.Array(T[0]),
        lambda f, a, /: tuple(filter(f, a)),
    ),
    "count": Function.new(
        (types.Function(T[0], types.boolean), types.Array(T[0])),
        types.integer,
        lambda f, a, /: sum(map(f, a)),
    ),
    "zip": Function.new(
        (types.Array(T[0]), types.Array(T[1])),
        types.Array(types.Tuple(T[0], T[1])),
        lambda x, y, /: tuple(zip(x, y)),
    ),
    "zip_with": Function.new(
        (types.Function.new((T[0], T[1]), T[2]), types.Array(T[0]), types.Array(T[1])),
        types.Array(T[2]),
        lambda f, x, y, /: tuple(map(lambda i, j, /: f(i)(j), x, y)),
    ),
    "unique": Function.new(
        (types.Array(EQ),),
        types.Array(EQ),
        # dict instead of set preserves order
        lambda a, /: tuple(dict.fromkeys(a)),
    ),
    "sort": Function.new(
        (types.Array(OR),), types.Array(OR), lambda a, /: tuple(sorted(a))
    ),
    "sort_by": Function.new(
        (types.Function(T[0], OR), types.Array(T[0])),
        types.Array(T[0]),
        lambda f, a, /: tuple(sorted(a, key=f)),
    ),
}

#: A frozenset of every built-in name.
BUILTIN_NAMES = frozenset(BUILTINS) | frozenset({"/="})
# TODO Namespaces so these don't need an asterisk
PRELUDE = """
/=: x* -> y* -> not (= x* y*)
"""
