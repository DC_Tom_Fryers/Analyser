"""This module handles parsing code strings to syntax trees."""
from __future__ import annotations

import dataclasses
import re
from collections import deque
from collections.abc import Iterator
from dataclasses import dataclass
from typing import ClassVar, Protocol, TypeVar, overload

from analyser.scripts import nodes
from analyser.scripts.errors import ScriptParseError, ScriptTokenisationError


@dataclass(frozen=True)
class Token:
    token_type_name: ClassVar[str]
    pos: slice = dataclasses.field(compare=False)


class TokenCreator(Protocol):
    def __call__(self, pos: slice, *args: str, **kwargs: str) -> Token:
        ...


class Newline(Token):
    token_type_name = "new line"


class Colon(Token):
    token_type_name = "':'"


class Dot(Token):
    token_type_name = "'.'"


class OpenBracket(Token):
    token_type_name = "'('"


class CloseBracket(Token):
    token_type_name = "')'"


class OpenSquareBracket(Token):
    token_type_name = "'['"


class CloseSquareBracket(Token):
    token_type_name = "']'"


class Arrow(Token):
    token_type_name = "'->'"


class Comma(Token):
    token_type_name = "','"


@dataclass(frozen=True)
class Integer(Token):
    token_type_name = "integer literal"
    text: str

    @property
    def value(self) -> int:
        return int(self.text)


@dataclass(frozen=True)
class Float(Token):
    token_type_name = "float literal"
    text: str

    @property
    def value(self) -> float:
        return float(self.text)


@dataclass(frozen=True)
class String(Token):
    token_type_name = "string literal"
    text: str


@dataclass(frozen=True)
class IfThenElse(Token):
    pass


@dataclass(frozen=True)
class If(IfThenElse):
    token_type_name = "if"


@dataclass(frozen=True)
class Then(IfThenElse):
    token_type_name = "then"


@dataclass(frozen=True)
class Else(IfThenElse):
    token_type_name = "else"


@dataclass(frozen=True)
class Identifier(Token):
    token_type_name = "identifier"
    name: str


@dataclass(frozen=True)
class Comment(Token):
    token_type_name = "comment"
    content: str


@dataclass(frozen=True)
class EOF(Token):
    token_type_name = "EOF"


CHAR_TOKENS = {
    "(": OpenBracket,
    ")": CloseBracket,
    "\n": Newline,
    ":": Colon,
    ",": Comma,
    "[": OpenSquareBracket,
    "]": CloseSquareBracket,
    ".": Dot,
}

INTEGER_RE = re.compile(r"(-?[0-9]+)")
FLOAT_RE = re.compile(r"(-?[0-9]+\.[0-9]*)")
TOKEN_TYPES: tuple[tuple[re.Pattern[str], TokenCreator], ...] = tuple(
    (re.compile(r), t)
    for r, t in (
        (r"->", Arrow),
        (FLOAT_RE.pattern, Float),
        (INTEGER_RE.pattern, Integer),
        (r"if", If),
        (r"then", Then),
        (r"else", Else),
        (r"([a-zA-Z_+\-*/&'=<>|?][a-zA-Z_+\-*/&'=<>0-9|?]*)", Identifier),
        (r"#(.*)", Comment),
        (r'"((?:\\n|\\\\|\\"|[^"\\])*)"', String),
        (r"\r\n", Newline),
    )
)

ESCAPES = {"\n": "\\n", '"': '\\"', "\\": "\\\\"}


def escape(string: str, /) -> str:
    r"""
    Add escape sequences to encode special characters in a string.

    Newlines are replaced with ``\n``, and backslashes and double quotes
    are preceded with a backslash.
    """
    return "".join(ESCAPES.get(c, c) for c in string)


def _unescape(string: str, /) -> Iterator[str]:
    chars = iter(string)
    for char in chars:
        if char != "\\":
            yield char
            continue
        try:
            char = next(chars)
            yield next(c for c, e in ESCAPES.items() if e == rf"\{char}")
        except (StopIteration, IndexError):
            raise ScriptParseError(f'Invalid string literal: "{string}".') from None


def unescape(string: str, /) -> str:
    """
    Decode the escape sequences present in a string.

    This is the (left) inverse of `escape`.

    :raises ScriptParseError: The input contains invalid escape sequences.
    """
    return "".join(_unescape(string))


_T = TypeVar("_T", bound="Token")


@dataclass
class TokenList:
    __slots__ = ("tokens",)
    tokens: deque[Token]

    @staticmethod
    def ignored(token: Token, /, *, skip_nl: bool = True, skip_cm: bool = True) -> bool:
        return (skip_nl and isinstance(token, Newline)) or (
            skip_cm and isinstance(token, Comment)
        )

    def peek(self, ahead: int = 0, **skips: bool) -> Token | None:
        """
        Get the token some number of tokens ahead.

        Optionally ignore commments and newlines. Returns None at EOF.
        """
        index = 0
        for token in self.tokens:
            if self.ignored(token, **skips):
                continue
            if index == ahead:
                return token
            index += 1

        return None

    @overload
    def pop(self, types: type[_T], **skips: bool) -> _T:
        ...

    @overload
    def pop(self, types: tuple[type[Token], ...], **skips: bool) -> Token:
        ...

    def pop(self, types: type[Token] | tuple[type[Token], ...], **skips: bool) -> Token:
        """
        Get and remove the next token.

        Optionally ignore comments and newlines.

        :raises ScriptParseError: The token's type is not in types.
        """
        while True:
            try:
                token = self.tokens.popleft()
            except IndexError:
                token = EOF(slice(0))
            if not self.ignored(token, **skips):
                break
        if not isinstance(token, types):
            if isinstance(types, tuple):
                expected = f"one of {','.join(t.token_type_name for t in types)}"
            else:
                expected = types.token_type_name

            raise ScriptParseError(
                f"Invalid token. Expected {expected}, but got {token.token_type_name}."
            )
        return token

    def trim(self, **skips: bool) -> None:
        """Remove all leading comments and newline tokens."""
        while self and self.ignored(self.tokens[0], **skips):
            self.tokens.popleft()

    def __bool__(self) -> bool:
        return bool(self.tokens)

    def return_expression(self, parts: list[nodes.Expression]) -> nodes.Expression:
        """
        Convert an list of values to a function call or value.

        :return:
            A (curried) `scripts.nodes.FunctionCall` if there are at least
            two tokens, otherwise just the single token unmodified.
        """
        if len(parts) == 0:
            raise ScriptParseError("Empty expression.")
        if len(parts) == 1:
            return parts[0]
        result = nodes.FunctionCall(
            slice(parts[0].pos.start, parts[1].pos.stop), *parts[:2]
        )
        for part in parts[2:]:
            result = nodes.FunctionCall(
                slice(result.pos.start, part.pos.stop), result, part
            )
        return result

    def parse_expression(self, line_can_end: bool = False) -> nodes.Expression:
        if isinstance(self.peek(1), Arrow):
            return self.parse_function(line_can_end=line_can_end)
        parts: list[nodes.Expression] = []
        token: Token
        while True:
            next_token = self.peek()
            if next_token is None or (
                parts and line_can_end and isinstance(self.peek(skip_nl=False), Newline)
            ):
                return self.return_expression(parts)
            if isinstance(next_token, Identifier):
                parts.append(self.parse_identifier())
            elif isinstance(next_token, OpenBracket):
                parts.append(self.parse_bracketed())
            elif isinstance(next_token, (CloseBracket, Comma, CloseSquareBracket)):
                return self.return_expression(parts)
            elif isinstance(next_token, Integer):
                token = self.pop(Integer)
                parts.append(nodes.Integer(token.pos, token.value))
            elif isinstance(next_token, Float):
                token = self.pop(Float)
                parts.append(nodes.Float(token.pos, token.value))
            elif isinstance(next_token, String):
                parts.append(self.parse_string())
            elif isinstance(next_token, IfThenElse):
                if isinstance(next_token, If):
                    parts.append(self.parse_if_then_else(line_can_end=line_can_end))
                else:
                    return self.return_expression(parts)
            elif isinstance(next_token, OpenSquareBracket):
                parts.append(self.parse_array())
            elif isinstance(next_token, Dot) and parts:
                self.pop(Dot)
                if isinstance(self.peek(), Dot):
                    self.pop(Dot)
                    part = 1
                else:
                    part = 0
                attribute = self.pop(Integer)
                attribute_node = nodes.Integer(attribute.pos, attribute.value)

                parts[-1] = nodes.AttributeAccess(
                    slice(parts[-1].pos.start, attribute.pos.stop),
                    parts[-1],
                    attribute_node,
                    part,
                )
            else:
                raise ScriptParseError(f"Invalid token in expression: {next_token}.")

    def parse_string(self) -> nodes.String:
        token = self.pop(String)
        return nodes.String(token.pos, unescape(token.text))

    def parse_if_then_else(self, line_can_end: bool = False) -> nodes.IfThenElse:
        start = self.pop(If).pos.start
        cond = self.parse_expression()
        self.pop(Then)
        true_val = self.parse_expression()
        self.pop(Else)
        false_val = self.parse_expression(line_can_end=line_can_end)
        return nodes.IfThenElse(
            slice(start, false_val.pos.stop), cond, true_val, false_val
        )

    def parse_identifier(self) -> nodes.Identifier:
        token = self.pop(Identifier)
        return nodes.Identifier(token.pos, token.name)

    def parse_array(self) -> nodes.Array:
        start = self.pop(OpenSquareBracket).pos.start
        # Handle empty array
        if isinstance(self.peek(), CloseSquareBracket):
            end = self.pop(CloseSquareBracket).pos.stop
            return nodes.Array(slice(start, end), ())

        values: list[nodes.Expression] = []
        while True:
            if not self:
                raise ScriptParseError("Unclosed square bracket.")
            if isinstance(self.peek(), CloseSquareBracket):
                break
            values.append(self.parse_expression())
            if isinstance(self.peek(), CloseSquareBracket):
                break
            self.pop(Comma)
        end = self.pop(CloseSquareBracket).pos.stop
        return nodes.Array(slice(start, end), tuple(values))

    def parse_bracketed(self) -> nodes.Expression:
        """
        Parse a bracketed expression.

        :return:
            Either a `scripts.nodes.FunctionCall` or `scripts.nodes.Tuple`,
            depending on the presence of a comma.
        """
        start = self.pop(OpenBracket).pos.start
        if isinstance(self.peek(), CloseBracket):
            raise ScriptParseError("Empty expression")
        parts = [self.parse_expression()]
        while isinstance(self.peek(), Comma):
            self.pop(Comma)
            if isinstance(self.peek(), CloseBracket):
                break
            parts.append(self.parse_expression())

        pos = slice(start, self.pop(CloseBracket).pos.stop)
        if len(parts) > 1:
            return nodes.Tuple(pos, tuple(parts))

        return dataclasses.replace(parts[0], pos=pos)

    def parse_function(self, line_can_end: bool = False) -> nodes.Function:
        argument = self.parse_identifier()
        self.pop(Arrow)
        body = self.parse_expression(line_can_end=line_can_end)
        return nodes.Function(slice(argument.pos.start, body.pos.stop), argument, body)

    def parse_assignment(self) -> nodes.Assignment:
        identifier = self.parse_identifier()
        self.pop(Colon)
        expression = self.parse_expression(line_can_end=True)
        if isinstance(self.peek(skip_nl=False), Newline):
            stop = self.pop(Newline, skip_nl=False).pos.stop
        else:
            stop = expression.pos.stop

        return nodes.Assignment(
            slice(identifier.pos.start, stop), identifier, expression
        )

    def parse_statement(self) -> nodes.Statement:
        if isinstance(self.peek(skip_cm=False), Comment):
            comment = self.pop(Comment, skip_cm=False)
            return nodes.Comment(comment.pos, comment.content)
        if isinstance(self.peek(1), Colon):
            return self.parse_assignment()
        raise ScriptParseError("Cannot parse statement.")

    def parse_script(self) -> nodes.Script:
        if not self:
            return nodes.Script(slice(0), ())
        statements = []
        pos = slice(self.tokens[-1].pos.stop)
        self.trim(skip_cm=False)
        while self:
            statements.append(self.parse_statement())
            self.trim(skip_cm=False)
        return nodes.Script(pos, tuple(statements))


def tokens(code: str, /) -> Iterator[Token]:
    position = 0
    end = len(code)
    while position < end:
        if code[position] == " ":
            position += 1
            continue
        if code[position] in CHAR_TOKENS:
            yield CHAR_TOKENS[code[position]](
                slice(position, position := (position + 1))
            )
            continue

        for regex, token_type in TOKEN_TYPES:
            if match := regex.match(code, position):
                yield token_type(
                    slice(position, position := match.end()), *match.groups()
                )
                break
        else:
            preview = code[:30] if len(code) <= 30 else f"{code[:27]}..."
            raise ScriptTokenisationError(f'Cannot tokenise "{preview}".')


def parse_script(script: str, /) -> nodes.Script:
    """
    Parse a script string to a script node.

    :raises ScriptParseError: The script cannot be parsed.
    """
    return TokenList(deque(tokens(script))).parse_script()
