from __future__ import annotations

from collections import defaultdict
from collections.abc import Iterator, Mapping
from contextlib import suppress
from dataclasses import dataclass
from typing import cast

from analyser.scripts import nodes, types
from analyser.scripts.errors import ScriptError, ScriptNameError, ScriptTypeError
from analyser.scripts.types import Type, Typeclass

LITERAL_TYPES = {
    nodes.Integer: types.integer,
    nodes.String: types.string,
    nodes.Float: types.float_,
}


def assignments(script: nodes.Script) -> Iterator[tuple[str, nodes.Assignment]]:
    """
    Iterate over the assignment nodes in a script.

    :return: an iterator of (name, assignment_node)
    """
    for statement in script.statements:
        if isinstance(statement, nodes.Assignment):
            yield statement.identifier.name, statement


@dataclass(init=False)
class Inferrer:
    __slots__ = (
        "variables",
        "classes",
        "nodes",
        "inferring",
        "error_stack",
        "polymorphic_uses",
        "polymorphic_depends",
        "defining_name",
    )
    variables: dict[str, Type]
    classes: dict[str, Typeclass]

    def __init__(
        self, variables: dict[str, Type], classes: dict[str, Typeclass]
    ) -> None:
        self.variables = variables
        self.classes = classes
        self.nodes: dict[int, Type] = {}
        self.inferring: set[str] = set()
        self.error_stack: list[nodes.Node] = []
        self.polymorphic_uses: Mapping[str, list[nodes.Identifier]] = defaultdict(list)
        self.polymorphic_depends: defaultdict[
            str, list[nodes.Identifier]
        ] = defaultdict(list)

    def update_types(self, restriction: dict[types.Var, Type]) -> bool:
        """
        Update all variable types to follow a restriction.

        :return: bool(restriction)
        """
        if not restriction:
            return False
        # I think it is OK to do all variables here, but restricting to
        # those in self.inferring is faster.
        for variable in self.inferring:
            type_ = self.variables[variable]
            if type_.variables:
                self.variables[variable] = type_.substitute(restriction)
        self.nodes = {
            n: t.substitute(restriction) if t.variables else t
            for n, t in self.nodes.items()
        }
        return True

    def infer(self, expr: nodes.Expression, /) -> Type:
        """Determine the type of an expression."""
        try:
            if self.inferring is None:
                self.inferring = set()
            type_ = self._infer(expr)
            self.nodes[id(expr)] = type_
            return type_
        except Exception:
            self.error_stack.append(expr)
            raise

    def _infer(self, expr: nodes.Expression, /) -> Type:
        with suppress(KeyError):
            return LITERAL_TYPES[type(expr)]
        if isinstance(expr, nodes.FunctionCall):
            return self.infer_function_call(expr)
        if isinstance(expr, nodes.Identifier):
            return self.infer_identifier(expr)
        if isinstance(expr, nodes.Array):
            return self.infer_array(expr)
        if isinstance(expr, nodes.Tuple):
            return self.infer_tuple(expr)
        if isinstance(expr, nodes.AttributeAccess):
            return self.infer_attribute_access(expr)
        if isinstance(expr, nodes.Function):
            return self.infer_function(expr)
        if isinstance(expr, nodes.IfThenElse):
            return self.infer_if_then_else(expr)
        raise NotImplementedError(f"unable to infer type of {expr}")

    def infer_function_call(self, expr: nodes.FunctionCall, /) -> Type:
        function_type = self.infer(expr.function)
        argument_type = self.infer(expr.argument)
        function_type = self.nodes[id(expr.function)]
        # General function
        if isinstance(function_type, types.Var):
            new_type = types.Function(argument_type, types.Var())
            restriction = function_type.restricted(new_type, self.classes)
            function_type = new_type
        elif isinstance(function_type, types.Function):
            restriction = function_type.parameter_type.restricted(
                argument_type, self.classes
            )
            function_type = function_type.substitute(restriction)
        else:
            raise ScriptTypeError(f"{function_type} is not a function.")
        self.update_types(restriction)
        return cast(types.Function, function_type).return_type

    def infer_identifier(self, expr: nodes.Identifier, /) -> Type:
        try:
            type_ = self.variables[expr.name]
        except KeyError:
            raise ScriptNameError(f"Unknown name: {expr.name}.") from None
        if expr.name not in self.inferring:
            type_map = {i: i.copy() for i in type_.variables}
            if type_map:
                self.polymorphic_uses[expr.name].append(expr)
                type_ = type_.substitute(type_map)
                self.polymorphic_depends[self.defining_name].append(expr)
        return type_

    def infer_array(self, expr: nodes.Array, /) -> types.Array:
        entry_types = [self.infer(e) for e in expr.values]
        # Ensure all entries have the same type
        try:
            entry_type: Type = entry_types.pop(0)
        except IndexError:
            return types.Array(types.Var())
        for other_entry_type in entry_types:
            restriction = entry_type.restricted(other_entry_type, self.classes)
            if self.update_types(restriction):
                entry_type = entry_type.substitute(restriction)
        return types.Array(entry_type)

    def infer_tuple(self, expr: nodes.Tuple, /) -> types.Tuple:
        entry_types = [self.infer(e) for e in expr.values]
        type_ = types.Tuple(entry_types.pop(-2), entry_types.pop())
        while entry_types:
            type_ = types.Tuple(entry_types.pop(), type_)
        return type_

    def infer_attribute_access(self, expr: nodes.AttributeAccess, /) -> Type:
        value_type = self.infer(expr.value)
        index = expr.attribute.value
        while True:
            restriction = value_type.restricted(
                types.Tuple(types.Var(), types.Var()), self.classes
            )
            if self.update_types(restriction):
                value_type = value_type.substitute(restriction)
            value_type = cast(types.Tuple, value_type)
            if index == 0:
                return value_type.types[expr.part]
            value_type = value_type.types[1]
            index -= 1

    def infer_function(self, expr: nodes.Function, /) -> types.Function:
        if expr.parameter.name in self.variables:
            raise ScriptNameError(f"{expr.parameter.name} is already defined.")
        self.variables[expr.parameter.name] = types.Var()
        # Add a new variable to infer: the parameter type.
        self.inferring.add(expr.parameter.name)
        return_type = self.infer(expr.body)
        parameter_type = self.variables.pop(expr.parameter.name)
        # Stop trying to infer the parameter type because it is now out
        # of scope.
        self.inferring.remove(expr.parameter.name)
        return types.Function(parameter_type, return_type)

    def infer_if_then_else(self, expr: nodes.IfThenElse, /) -> Type:
        cond_type = self.infer(expr.cond)
        restriction = cond_type.restricted(types.boolean, self.classes)
        self.update_types(restriction)
        true_type = self.infer(expr.true_val)
        false_type = self.infer(expr.false_val)
        restriction = true_type.restricted(false_type, self.classes)
        if self.update_types(restriction):
            true_type = true_type.substitute(restriction)
        return true_type

    def add_script_variables(self, script: nodes.Script) -> None:
        """
        Add any variables defined in the script to self.variables.

        Any variable in the script is added to self.variables as a
        types.Var().

        :raises ScriptNameError: A variable is defined more than once.
        """
        for name, statement in assignments(script):
            if name in self.variables:
                self.error_stack = [statement.identifier, statement]
                raise ScriptNameError(f"{name} is already defined.")
            self.variables[name] = types.Var()

    def track_previous_uses(self, script: nodes.Script) -> None:
        """
        Go back over the script, making types compatible.

        This lets things like

        ::

            x: y
            y: 0

        work.

        This also ensures an entry exists for every defined name in
        self.polymorphic_uses, which is necessary.
        """
        for name, _ in assignments(script):
            for use in self.polymorphic_uses[name]:
                use_type = self.nodes[id(use)]
                try:
                    restriction = use_type.restricted(
                        self.variables[name], self.classes
                    )
                except ScriptTypeError:
                    self.error_stack = [use]
                    raise
                restriction = {
                    v: t
                    for v, t in restriction.items()
                    if v not in self.variables[name].variables
                }
                self.update_types(restriction)

    def _infer_script(self, script: nodes.Script) -> None:
        self.add_script_variables(script)
        for self.defining_name, statement in assignments(script):
            self.defining_name = statement.identifier.name
            self.inferring.add(self.defining_name)
            type_ = self.infer(statement.expression)
            try:
                restriction = type_.restricted(
                    self.variables[self.defining_name], self.classes
                )
            except ScriptTypeError:
                self.error_stack = [statement]
                raise
            self.update_types(restriction)
            self.inferring.remove(self.defining_name)
        self.inferring = set(self.variables)
        self.track_previous_uses(script)
        self.polymorphic_uses = {
            n: u
            for n, u in self.polymorphic_uses.items()
            if self.variables[n].variables
        }

    def infer_script(self, script: nodes.Script) -> None:
        """Determine the type of every node in the script."""
        try:
            self._infer_script(script)
        except ScriptError as error:
            error.error_stack = self.error_stack
            raise
