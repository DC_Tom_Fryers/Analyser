"""
This package provides functions for running scripts.

Types
^^^^^

Builtin Types
'''''''''''''

``Boolean``, ``Integer`` and ``Float``, ``String`` are represented in
Python code by their obvious equivalents ``bool``, ``int``, ``float``
and ``str``.  ``Array`` and ``Tuple`` both correspond to Python's
``tuple``, and functions are represented by callable objects.

Optional types are ``None`` if ``none``. Otherwise, their value is
wrapped in a single-element ``tuple``.

Typeclasses
'''''''''''

To allow more optimisations, there are some restrictions on the
behaviour of ``Equatable`` and ``Ordered`` values.

The Python representations of any ``Equatable`` values passed in must
support an ``__eq__`` method as an equivalent to calling the typeclass
implementation directly.  They must also be hashable.

Similarly, any Python representation of an ``Ordered`` value must
support ``__lt__``, ``__gt__``, ``__le__`` and ``__ge__``, and these
must be equivalent to the provided implementations.

The recommended way to satisfy these requirements is to implement the
dunder methods directly, and to use functions from the ``operator``
module as the typeclass implementations.
"""
from __future__ import annotations

import itertools
from typing import Any

from analyser.scripts import builtin, compile_, functions, infer, nodes, parse, types
from analyser.scripts.errors import ScriptRecursionError
from analyser.scripts.types import Type, Typeclass, Variable

__all__ = ["builtin", "functions", "nodes", "parse", "run", "run_string", "types"]


def merge_vars(
    additional_variables: dict[str, Variable] | None = None,
    additional_classes: None
    | (dict[str, tuple[frozenset[str] | None, dict[Type, dict[str, Variable]]]]) = None,
) -> tuple[dict[str, Variable], dict[str, Typeclass]]:
    """Add sets of additional variables and classes to the builtins."""
    variables = builtin.BUILTINS.copy()
    if additional_variables is not None:
        for name, val in additional_variables.items():
            if name in variables:
                raise ValueError(f"{name} is already defined")
            if name in {"if", "then", "else", "->"}:
                raise ValueError(f"invalid name: {name}")
            variables[name] = val
    classes = builtin.CLASSES.copy()
    if additional_classes is not None:
        for name, (requires, implementations) in additional_classes.items():
            if name not in classes:
                if requires is None:
                    raise TypeError(
                        "must provide typeclass dependencies for a new typeclass"
                    )
                classes[name] = Typeclass(requires, implementations)
                continue
            if not set(implementations).isdisjoint(classes[name].implementations):
                raise TypeError(f"cannot reimplement {name}")
            if requires not in {None, classes[name].requires}:
                raise TypeError(
                    "incompatible typeclass dependencies"
                    f" ({requires} != {classes[name].requires})"
                )
            classes[name] = Typeclass(
                classes[name].requires,
                {**implementations, **classes[name].implementations},
            )
    return variables, classes


def run(
    *script_nodes: nodes.Script,
    additional_variables: dict[str, Variable] | None = None,
    additional_classes: None
    | (dict[str, tuple[frozenset[str] | None, dict[Type, dict[str, Variable]]]]) = None,
) -> dict[str, Variable]:
    """
    Run a parsed script with variables added to the builtins.

    :param additional_variables:
        A dictionary mapping each name to an `scripts.types.Variable`.
    :param additional_classes:
        A dictionary mapping class names to implementation information.
        This is to be added to the builtin classes and implemenations
        before running.

    :return:
        A dictionary giving the types and values of the variables after
        the script has run.

    :raises ScriptTypeError: There is a type error in the script.
    :raises ScriptNameError:
        A name is defined twice or used without being defined.
    :raises ScriptRecursionError:
        Running the script exceeded Python's recursion limit.
    """
    script = nodes.Script(
        script_nodes[0].pos,
        tuple(
            itertools.chain(PRELUDE.statements, *(s.statements for s in script_nodes))
        ),
    )
    variables, classes = merge_vars(additional_variables, additional_classes)
    inferrer = infer.Inferrer({n: v.type_ for n, v in variables.items()}, classes)
    inferrer.infer_script(script)
    raw_variables = {n: v.value for n, v in variables.items()}
    compiler = compile_.Compiler(inferrer, raw_variables, classes)
    compiled = compiler.compile_(script)

    encoded_variables: dict[str, Any] = {"__builtins__": {}}
    for name, value in raw_variables.items():
        name = compiler.encode_name(name)
        if isinstance(value, functions.Function):
            encoded_variables[name] = value.curried_function
            encoded_variables["r" + name] = value.raw_function
        else:
            encoded_variables[name] = value
    for class_ in classes.values():
        for type_, implementation in class_.implementations.items():
            for name, function in implementation.items():
                value = function.value
                name = compiler.method_name(name, type_)
                if isinstance(value, functions.Function):
                    encoded_variables[name] = value.curried_function
                    encoded_variables["r" + name] = value.raw_function
                else:
                    encoded_variables[name] = value
    try:
        exec(compiled, encoded_variables)
    except RecursionError:
        raise ScriptRecursionError("Recursion limit exceeded.") from None
    raw_variables = {}
    for name, value in encoded_variables.items():
        if name == "__builtins__" or name[0] in compile_.SPECIAL_PREFIXES:
            continue
        name = compiler.decode_name(name)
        if name not in raw_variables:
            raw_variables[name] = value

    return {n: Variable(inferrer.variables[n], v) for n, v in raw_variables.items()}


def run_string(script: str, /) -> dict[str, Variable]:
    """
    Parse and run a script.

    :param script: The script to run as a string.

    :return:
        A dictionary giving the types and values of the variables after
        the script has run.

    :raises ScriptParseError: The script cannot be parsed.
    :raises ScriptTypeError: There is a type error in the script.
    :raises ScriptNameError:
        A name is defined twice or used without being defined.
    :raises ScriptRecursionError:
        Running the script exceeded Python's recursion limit.
    """
    return run(parse.parse_script(script))


PRELUDE = parse.parse_script(builtin.PRELUDE)
