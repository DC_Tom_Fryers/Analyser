from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass
from functools import partial
from typing import Any

from analyser.scripts import types
from analyser.scripts.types import Variable


def curry(function: Callable[..., Any], arg_count: int) -> Callable[[Any], Any]:
    """
    Curry a function with arg_count parameters.

    arg_count must be at least 2.

    >>> add = curry(lambda x, y, z: x + y + z, 3)
    >>> add(1)(2)(3)
    6
    """
    if arg_count == 2:
        return lambda x, /: partial(function, x)
    return lambda x, /: curry(partial(function, x), arg_count - 1)


@dataclass(frozen=True)
class Function:
    """A function wrapper."""

    #: The parameter count function.
    raw_params: int
    #: The original, uncurried function.
    raw_function: Callable[..., Any]
    #: The curried function.
    curried_function: Callable[[Any], Any]

    @classmethod
    def new(
        cls,
        param_types: tuple[types.Type, ...],
        return_type: types.Type,
        function: Callable[..., Any],
    ) -> Variable:
        """Create a `Function` from type information and a callable."""
        type_ = types.Function.new(param_types, return_type)
        if len(param_types) > 1:
            return Variable(
                type_,
                cls(len(param_types), function, curry(function, len(param_types))),
            )
        return Variable(type_, cls(1, function, function))


@dataclass(frozen=True)
class Method:
    r"""
    A typeclass method.

    :param class\_: The typeclass to which the method belongs.
    """

    __slots__ = ("class_",)
    class_: str
