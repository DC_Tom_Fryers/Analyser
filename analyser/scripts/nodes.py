from __future__ import annotations

from dataclasses import dataclass


@dataclass(frozen=True)
class Node:
    __slots__ = ("pos",)
    pos: slice

    @property
    def children(self) -> tuple[Node, ...]:
        ...


@dataclass(frozen=True)
class Expression(Node):
    __slots__ = ()

    @property
    def children(self) -> tuple[Expression, ...]:
        ...


@dataclass(frozen=True)
class Identifier(Expression):
    __slots__ = ("name",)
    name: str

    @property
    def children(self) -> tuple[()]:
        return ()


@dataclass(frozen=True)
class Literal(Expression):
    __slots__ = ("value",)
    value: object

    @property
    def children(self) -> tuple[()]:
        return ()


@dataclass(frozen=True)
class Integer(Literal):
    __slots__ = ()
    value: int


@dataclass(frozen=True)
class Float(Literal):
    __slots__ = ()
    value: float


@dataclass(frozen=True)
class String(Literal):
    __slots__ = ()
    value: str


@dataclass(frozen=True)
class ArrayTuple(Expression):
    __slots__ = ("values",)
    values: tuple[Expression, ...]

    @property
    def children(self) -> tuple[Expression, ...]:
        return self.values


@dataclass(frozen=True)
class Array(ArrayTuple):
    __slots__ = ()


@dataclass(frozen=True)
class Tuple(ArrayTuple):
    __slots__ = ()


@dataclass(frozen=True)
class Function(Expression):
    __slots__ = ("parameter", "body")
    parameter: Identifier
    body: Expression

    @property
    def children(self) -> tuple[Identifier, Expression]:
        return (self.parameter, self.body)


@dataclass(frozen=True)
class FunctionCall(Expression):
    __slots__ = ("function", "argument")
    function: Expression
    argument: Expression

    @property
    def children(self) -> tuple[Expression, Expression]:
        return (self.function, self.argument)


@dataclass(frozen=True)
class AttributeAccess(Expression):
    __slots__ = ("value", "attribute", "part")
    value: Expression
    attribute: Integer
    part: int

    @property
    def children(self) -> tuple[Expression, Integer]:
        return (self.value, self.attribute)


@dataclass(frozen=True)
class IfThenElse(Expression):
    __slots__ = ("cond", "true_val", "false_val")
    cond: Expression
    true_val: Expression
    false_val: Expression

    @property
    def children(self) -> tuple[Expression, Expression, Expression]:
        return (self.cond, self.true_val, self.false_val)


@dataclass(frozen=True)
class Script(Node):
    __slots__ = ("statements",)
    statements: tuple[Statement, ...]

    @property
    def children(self) -> tuple[Statement, ...]:
        return self.statements


@dataclass(frozen=True)
class Statement(Node):
    __slots__ = ()


@dataclass(frozen=True)
class Comment(Statement):
    __slots__ = ("content",)
    content: str

    @property
    def children(self) -> tuple[()]:
        return ()


@dataclass(frozen=True)
class Assignment(Statement):
    __slots__ = ("identifier", "expression")
    identifier: Identifier
    expression: Expression

    @property
    def children(self) -> tuple[Identifier, Expression]:
        return (self.identifier, self.expression)
