from __future__ import annotations

from analyser.scripts import nodes


class ScriptError(Exception):
    """Base class for errors caused by invalid code."""

    error_stack: list[nodes.Node]


class ScriptParseError(ScriptError):
    """Script could not be parsed."""


class ScriptTokenisationError(ScriptParseError):
    """Script could not be tokenised."""


class ScriptTypeError(ScriptError):
    """Incompatible types in script."""


class ScriptNameError(ScriptError):
    """Name defined twice or used undefined."""


class ScriptRecursionError(ScriptError):
    """Maximum recursion depth exceeded."""
