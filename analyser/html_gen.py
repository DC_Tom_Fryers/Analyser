from __future__ import annotations

import html
from collections.abc import Callable, Mapping


def css(properties: Mapping[str, object], /) -> str:
    return "; ".join(f"{p}: {v}" for p, v in properties.items())


class _Tagger:
    __slots__ = ()

    def __getattr__(self, tag_name: str, /) -> Callable[..., str]:
        def function(
            content: object = None,
            /,
            style: str | Mapping[str, object] | None = None,
            **attributes: str,
        ) -> str:
            if style is not None:
                attributes["style"] = style if isinstance(style, str) else css(style)

            attributes_string = "".join(
                f' {a[:-1] if a.endswith("_") else a}="{html.escape(v)}"'
                if v != ""
                else f" {a}"
                for a, v in attributes.items()
            )

            if content is None:
                return f"<{tag_name}{attributes_string} />"
            return f"<{tag_name}{attributes_string}>{content}</{tag_name}>"

        return function


tag = _Tagger()
