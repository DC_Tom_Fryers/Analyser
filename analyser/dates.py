"""This module provides date parsing functions."""
from __future__ import annotations

import re
from contextlib import suppress
from datetime import date

DAY_NUM = r"(?:0?[1-9]|[12]\d|3[01])"
MONTH_NUM = r"(?:0?[1-9]|1[0-2])"
YEAR_NUM = r"\d*"

MONTHS = (
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
)
MONTHS_3L = [m[:3] for m in MONTHS]
MONTH_MATCH = "|".join(MONTHS) + "|".join(MONTHS_3L)


def make_date_re(pattern: str, /) -> re.Pattern[str]:
    return re.compile(
        pattern.replace("MONTHS", MONTH_MATCH)
        .replace("DAY_NUM", DAY_NUM)
        .replace("MONTH_NUM", MONTH_NUM)
        .replace("YEAR_NUM", YEAR_NUM),
        re.ASCII,
    )


ISO_DATE_RE = re.compile(r"(?<!\d)\d{4}-\d{2}-\d{2}(?!\d)", re.ASCII)
UK_DATE_RE = make_date_re(r"(?<!\d)DAY_NUM ?[/.] ?MONTH_NUM ?[/.] ?YEAR_NUM(?!\d)")
# These match 1nd, 2rd, 3th, 4st etc. These strings are unlikely to
# appear anywhere often, so it's fine.
UK_WRITTEN_DATE_RE = make_date_re(
    r"(?<!\d)DAY_NUM(?:st|nd|rd|th)? (?:MONTHS|MONTH_NUM) YEAR_NUM(?!\d)"
)


def deday(string: str, /) -> int:
    return int(string.rstrip("stndrh"))


def demonth(string: str, /) -> int:
    with suppress(ValueError):
        return int(string)

    with suppress(ValueError):
        return MONTHS.index(string) + 1

    return MONTHS_3L.index(string) + 1


def deyear(string: str, /) -> int:
    if len(string) == 4:
        return int(string)
    if len(string) == 2:
        year = int(string)
        # 39 -> 2039, but 40 -> 1940
        if year < 40:
            return 2000 + year
        return 1900 + year
    raise ValueError("years must be two or four digits long")


def uk_format(string: str, /) -> date:
    parts = string.replace(" ", "").replace(".", "/").split("/")
    return date(deyear(parts[2]), int(parts[1]), int(parts[0]))


def uk_written_format(string: str, /) -> date:
    day, month, year = string.split()
    return date(deyear(year), demonth(month), deday(day))


DATE_FORMATS = (
    (ISO_DATE_RE, date.fromisoformat),
    (UK_DATE_RE, uk_format),
    (UK_WRITTEN_DATE_RE, uk_written_format),
)


def to_date(string: str, /) -> date:
    string = string.casefold()
    for regex, format_func in DATE_FORMATS:
        if date_string := regex.search(string):
            return format_func(date_string.group(0))
    raise ValueError(f"Cannot interpret {string!r} as a date")
