from __future__ import annotations

from collections.abc import Iterable, Iterator


def format_classes(classes: Iterable[str], /) -> str:
    class_string = " ".join(classes)
    return f' class="{class_string}"' if class_string else ""


def _table_to_html(
    table: Iterable[Iterable[str]],
    heading_classes: Iterable[Iterable[str]],
    classes: Iterable[str] = (),
) -> Iterator[str]:
    rows = iter(table)
    try:
        headings = next(rows)
    except StopIteration as error:
        raise ValueError("table has no headings") from error

    yield f"<table{format_classes(classes)}><thead><tr>"
    for heading, h_classes in zip(headings, heading_classes):
        yield f"<th{format_classes(h_classes)}>{heading}</th>"
    yield "</tr></thead><tbody>"
    for row in rows:
        yield "<tr>"
        yield from (f"<td>{i}</td>" for i in row)
        yield "</tr>"
    yield "</tbody></table>"


def table_to_html(
    table: Iterable[Iterable[str]],
    heading_classes: Iterable[Iterable[str]],
    classes: Iterable[str] = (),
) -> str:
    """
    Convert a table (iterable of iterables) to an HTML table.

    A 'class set' in the following description is an iterable of
    strings, with each element giving the name of a class.

    :param table: The table itself.
    :param heading_classes:
        An iterable of class sets to apply to each heading element.
    :param classes:
        An class set to apply to the whole table.
    """
    return "".join(_table_to_html(table, heading_classes, classes))
