#!/usr/bin/env python3
import analyser.ui

if __name__ == "__main__":
    analyser.ui.create_app().run()
