# Analyser

This is a web tool for analysing data.

## Usage Instructions

To install it, run `pip install --user .` in this directory. Then run
`server.py`, and navigate to `http://127.0.0.1:5000/` in your web
browser.

To add support for importing more formats, run
`pip install --user .[formats]`.

## Documentation

To build the documentation, first run

``` bash
sphinx-apidoc -o docs/source/api analyser
```

then run `docs/make.bat html` or `cd docs && make html`, depending on
your operating system. This will create HTML documentation in the
`docs/build/html` directory.

To update the documentation after making changes, it is necessary to
first delete the `docs/source/api` directory.
