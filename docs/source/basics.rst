******
Basics
******

Here is a simple script::

    hello: & "Hello, " "world!"

To run this, run

>>> from analyser import scripts
>>> scripts.run_string('hello: & "Hello, " "world!"')["hello"].value
'Hello, world!'

On the Python side, ``["hello"]`` specifies the name of the variable to
get (`analyser.scripts.run_string` returns a ``dict``). ``.value`` gets the value of
the variable, rather than the type. To get the type, use ``.type_``
instead.

The simple piece of code shows some features:

* Variable assignment, which uses a colon
* Strings are written with double quotes.
* Function call syntax, which unlike mathematical notation and some
  programming languages does not use brackets and commas.
  :math:`f(x, y, z)` is written as ::

      f x y z

  which has some advantages.

  Here ``&`` is the function, and ``"Hello, "`` and ``"world!"`` are the
  operands.

Here is a more complex example::

    appear_exactly_twice: a ->
      (filter
       (x -> = 2 (count (= x) a))
       (unique a))
    doubles: appear_exactly_twice [1, 2, 3, 4, 2, 1, 5, 4, 4]

Running this (whitespace is sometimes unimportant) should give:

>>> scripts.run_string("appear_exactly_twice: a -> (filter (x -> = 2 (count (= x) a)) (unique a))\n doubles: appear_exactly_twice [1, 2, 3, 4, 2, 1, 5, 4, 4]")["doubles"].value
(1, 2)

Here we see the syntax for creating arrays (``[a, b, c]``), functions
(``param -> value``) and the effects of more builtin functions. These
are documented at :doc:`builtins`.
