######################
Analyser Documentation
######################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/modules
   scripts

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
