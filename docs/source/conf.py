import re
import sys
from collections.abc import Iterable
from pathlib import Path

from pygments import token
from pygments.lexer import RegexLexer
from sphinx.highlighting import lexers

sys.path.insert(0, str(Path("../..").resolve()))

from analyser.scripts import builtin, parse  # noqa: E402

# Project information
project = "Analyser"
copyright = "2021, DataConsulting"  # noqa: A001
author = "Tom Fryers"

# General configuration
extensions = ["sphinx.ext.autodoc"]
templates_path = ["_templates"]
exclude_patterns = []
default_role = "py:obj"

# Options for HTML output
html_theme = "alabaster"
html_static_path = ["_static"]

# Highlighting setup
TOKEN_MAP = {
    parse.OpenBracket: token.Punctuation,
    parse.CloseBracket: token.Punctuation,
    parse.Arrow: token.Punctuation,
    parse.Float: token.Number.Float,
    parse.Integer: token.Number.Integer,
    parse.If: token.Keyword,
    parse.Then: token.Keyword,
    parse.Else: token.Keyword,
    parse.Identifier: token.Name.Variable,
    parse.Newline: token.Whitespace,
    parse.Colon: token.Punctuation,
    parse.Comma: token.Punctuation,
    parse.OpenSquareBracket: token.Punctuation,
    parse.CloseSquareBracket: token.Punctuation,
    parse.Comment: token.Comment.Single,
    parse.Dot: token.Punctuation,
    parse.String: token.String.Double,
}


def match_one_of(strings: Iterable[str]) -> str:
    return "|".join(re.escape(n) for n in sorted(strings, key=len, reverse=True))


tokens = (
    [(" +", token.Text)]
    + [(re.escape(char), TOKEN_MAP[token]) for char, token in parse.CHAR_TOKENS.items()]
    + [(regex.pattern, TOKEN_MAP[token]) for regex, token in parse.TOKEN_TYPES]
)

identifier_index = next(
    i for i, (_, t) in enumerate(tokens) if t is token.Name.Variable
)

tokens = (
    tokens[:identifier_index]
    + [
        (match_one_of(builtin.BUILTIN_NAMES), token.Name.Builtin),
        (
            match_one_of(
                ("Integer", "Boolean", "Float", "String", "Any")
                + tuple(builtin.CLASSES)
            ),
            token.Keyword.Type,
        ),
    ]
    + tokens[identifier_index:]
)


class ScriptLexer(RegexLexer):
    name = "analyser_scripts"

    tokens = {"root": tokens}


lexers["analyser_scripts"] = ScriptLexer()

highlight_language = "analyser_scripts"
