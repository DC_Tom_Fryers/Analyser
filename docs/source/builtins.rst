********
Builtins
********

These are the builtin functions available.

``Equatable`` Methods
=====================

``=``
-----

::

    Equatable 0 -> Equatable 0 -> Boolean

``=`` determines if two values are equal.

``Ordered`` Methods
===================

``<``
-----

::

    Ordered 0 -> Ordered 0 -> Boolean

Determine if the first argument is less than the second.

``>``
-----

::

    Ordered 0 -> Ordered 0 -> Boolean

Determine if the first argument is greater than the second.

``<=``
------

::

    Ordered 0 -> Ordered 0 -> Boolean

Determine if the first argument is less than or equal to the second.

``>=``
------

::

    Ordered 0 -> Ordered 0 -> Boolean

Determine if the first argument is greater than or equal to the second.

``Number`` Methods
==================

``+``
-----

::

    Number 0 -> Number 0 -> Number 0

Get the sum of two numbers.

``-``
-----

::

    Number 0 -> Number 0 -> Number 0

Get the difference of two numbers.

``*``
-----

::

    Number 0 -> Number 0 -> Number 0

Get the product of two numbers.

``Container`` Methods
=====================

``map``
-------

::

    (Any 0 -> Any 1) -> (Container 2) (Any 0) -> (Container 2) (Any 1)

Map a function over a container.

``Sequence`` Methods
====================

``&``
-----

::

    Sequence 0 -> Sequence 0 -> Sequence 0

Concatenate two sequences.

``length``
----------

::

    Sequence 0 -> Integer

Get the length of a sequence.

``reverse``
-----------

::

    Sequence 0 -> Sequence 0

Reverse a sequence.

``Representable`` Methods
=========================

``string``
----------

::

    Representable 0 -> String

Convert a value to a string.

``parse``
----------

::

    String -> Representable 0

Parse a value from a string.

Functions
=========

``/=``
------

::

    Equatable 0 -> Equatable 0 -> Boolean

``/=`` determines if two values are not equal.

``mod``
-------

::

    Integer -> Integer -> Boolean?

``mod x y`` returns ``x`` modulo ``y``, wrapped in an option. ``none``
is returned if ``y`` is zero.

``/``
-----

::

    Float -> Float -> Float

Get the quotient of two numbers.  May return ``nan``, ``inf`` or
``-inf`` in certain cases.

``sqrt``
--------

::

    Float -> Float

Get the square root of a float.

``exp``
-------

::

    Float -> Float

Get the value of :math:`e^x` for a given :math:`x`.

``float``
---------

::

    Integer -> Float

Convert an integer to a float.  Returns ``inf`` or ``-inf`` if the
integer is too large in magnitude.

``range``
---------

::

    Integer -> [Integer]

Get an array counting from zero up to (but excluding) the argument.

``case_fold``
-------------

::

    String -> String

Case fold a string.  This is similar to a lower case conversion, but
stronger.

``contains``
------------

::

    String -> String -> Boolean

Determine if a string (second argument) contains a substring (first
argument).

``join``
--------

::

    String -> [String] -> String

Join an array of strings to a string.

``id``
------

::

    Any 0 -> Any 0

Return the given argument unmodified.  This function is not very useful
by itself, but can be useful in conjunction with other functions.

``?``
-----

::

    Any 0 -> (Any 0)? -> Any 0

Unwrap an option type by providing a default.  If the second argument is
``some x``, ``x`` is returned.  Otherwise, if the second argument is
``none``, the first argument is returned.

``some``
--------

::

    Any 0 -> (Any 0)?

Create an option type from a normal value.

``not``
-------

::

    Boolean -> Boolean

Get the logical negation of a boolean value.

``and``
-------

::

    Boolean -> Boolean -> Boolean

Get the logical conjunction of two boolean values.  Short-circuits if
the first argument is ``false``.

``or``
------

::

    Boolean -> Boolean -> Boolean

Get the logical disjunction of two boolean values.  Short-circuits if
the first argument is ``true``.

``all``
-------

::

    [Boolean] -> Boolean

Determine if all elements of the array are ``true``.

``any``
-------

::

    [Boolean] -> Boolean

Determine if any elements of the array is ``true``.

``index``
---------

::

    [Any 0] -> Integer -> (Any 0)?

Get a (zero-indexed) entry of an array, wrapped in an option.  Returns
``none`` if the array is not long enough.

``head``
--------

::

    [Any 0] -> (Any 0)?

Get the first entry of an array, wrapped in an option.  Returns ``none``
if the array is empty.

``tail``
--------

::

    [Any 0] -> [Any 0]

Get all entries after the first in an array.  Returns an empty array if
the given array has fewer than two elements.

``take``
--------

::

    Integer -> [Any 0] -> [Any 0]

Get the beginning of an array.  The first parameter determines the
maximum number of elements to take.  If the array is too short, it is
returned unmodified.

``drop``
--------

::

    Integer -> [Any 0] -> [Any 0]

Get the end of an array.  The first parameter determines the maximum
number of elements to drop.  If the array is too short, an empty array
is returned.

``in``
------

::

    Any 0 -> [Any 0] -> Boolean

Determine if a value is in an array.

``reduce``
----------

::

    (Any 0 -> Any 0 -> Any 0) -> [Any 0] -> (Any 0)?

Combine the elements of an array to get a result.  This is easiest to
explain with examples. ::

    reduce + [0, 1, 2, 3, 4, 5]

and ::

    some (+ (+ (+ (+ (+ 0 1) 2) 3) 4) 5)

are equivalent, and ::

    reduce or [true, false, false, true]

and ::

    some (any [true, false, false, true])

are equivalent. ``reduce`` returns none if given an empty array.

``fold``
--------

::

    (Any 0 -> Any 1 -> Any 0) -> Any 0 -> [Any 1] -> Any 0

A more general version of ``reduce`` that takes a starting value (which
need not be the same type as the elements of the array) and combines
each elements of the array with it.

``filter``
----------

::

    (Any 0 -> Boolean) -> [Any 0] -> [Any 0]

Apply the given function to every element in the given array, returning
an array containing only those elements for which the function returned
``true``.

``count``
---------

::

    (Any 0 -> Boolean) -> [Any 0] -> Integer

Count the number of elements of the given array for which the given
function returns ``true``.

``zip``
-------

::

    [Any 0] -> [Any 1] -> [(Any 0, Any 1)]

Get an array of pairs of corresponding elements of the given arrays.
Stops at the length of the shortest given array.

``zip_with``
------------

::

    (Any 0 -> Any 1 -> Any 2) -> [Any 0] -> [Any 1] -> [Any 2]

Get an array of values of the given function applied to corresponding
elements of the given arrays.  Stops at the length of the shortest given
array.

This is equivalent to the following. ::

    f -> a -> b -> map (x -> f x.0 x..0) (zip a b)

``unique``
----------

::

    [Equatable 0] -> [Equatable 0]

Get an array with any duplicate elements removed.
