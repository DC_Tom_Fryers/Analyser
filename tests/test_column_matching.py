import pandas as pd

from analyser import column_match


def zero(_):
    return 0.0


def test_name_matching() -> None:
    data = pd.DataFrame({"thing's name": ["0"], "size": ["0"], "prices": ["0"]})
    mapping = column_match.match_columns(
        data,
        {
            "name": (zero, ("name",)),
            "size": (zero, ("size", "largeness")),
            "cost": (zero, ("cost", "price")),
        },
    )
    assert mapping == {"name": "thing's name", "size": "size", "cost": "prices"}


def test_value_matching() -> None:
    data = pd.DataFrame(
        {
            "invoice number": ["A193849B", "I000217"],
            "company": ["CoolCo Inc.", "OtherCo"],
            "cost": ["5.32", "1099.99"],
            "currency": ["GBP", "$"],
            "date": ["21/09/2021", "2023-02-01"],
            "time": ["17:11", "10:23 pm"],
            "description": ["This is a description for an automated test.", ""],
        }
    )
    mapping = column_match.match_columns(
        data,
        {
            "invoice number": (column_match.id_number_score, ()),
            "company": (column_match.company_score, ()),
            "cost": (column_match.money_score, ()),
            "currency": (column_match.currency_score, ()),
            "date": (column_match.date_score, ()),
            "time": (column_match.time_score, ()),
            "description": (column_match.english_text_score, ()),
        },
    )
    assert mapping == {
        c: c
        for c in (
            "invoice number",
            "company",
            "cost",
            "currency",
            "date",
            "time",
            "description",
        )
    }
