import tempfile

import pytest

import analyser.ui


@pytest.fixture
def client():
    with tempfile.TemporaryDirectory() as temp_dir:
        app = analyser.ui.create_app(session_data_path=temp_dir)
        yield app.test_client()
