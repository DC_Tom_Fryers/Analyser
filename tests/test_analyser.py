from __future__ import annotations

import re
from io import BytesIO


def test_index_page(client) -> None:
    index_page = client.get("/").data
    assert b"Click to proceed to the file upload page." in index_page


def test_upload_page(client) -> None:
    index_page = client.get("/upload").data
    assert b"Upload" in index_page


def upload(client, file: bytes, filename: str) -> bytes:
    with BytesIO(file) as f:
        return client.post("/setup", data={"file": (f, filename)}).data


CSV_FILE = b"""number,square,is prime,name
0,0,false,zero
1,1,false,one
2,4,true,two
3,9,true,three"""


def test_csv_upload(client) -> None:
    for filename in ("test_data.csv", "test data .csv", "ALL CAPS.CSV"):
        assert b"Setup" in upload(client, CSV_FILE, filename)


def test_tsv_upload(client) -> None:
    for filename in ("test_data.tsv", "\N{OX}.tsv", "tOGgLe CaSe.TaB"):
        assert b"Setup" in upload(client, CSV_FILE.replace(b",", b"\t"), filename)


JSON_FILE = b"""{
"colour": ["red", "green", "blue"],
"is_colour": ["true", "true", "true"],
}"""


def test_json_upload(client) -> None:
    assert b"Setup" in upload(client, JSON_FILE, "json.json")


def test_bad_json(client) -> None:
    assert b"Redirecting..." in upload(client, b"hi", "invalid_json.json")


def test_bad_extension(client) -> None:
    assert b"Redirecting..." in upload(client, b"irrelevant", "useless.jpg")


def find_action(setup_page: bytes, action: str) -> bytes:
    match = re.search(f'"({action}@.*?)"'.encode(), setup_page)
    assert match is not None
    return match.group(1)


def find_script(setup_page: bytes) -> bytes:
    match = re.search(b"<textarea .*?>(.*?)</textarea.*?>", setup_page, re.DOTALL)
    assert match is not None
    return match.group(1).strip()


def test_editing(client) -> None:
    expression = "map + 3 [1, 2, 3]"
    setup_page = upload(client, CSV_FILE, "edit_test.csv")
    setup_page = client.post(
        "/setup",
        data={"script": find_script(setup_page), find_action(setup_page, "extend"): ""},
    ).data

    setup_page = client.post(
        "/setup",
        data={
            "script": find_script(setup_page),
            find_action(setup_page, "new"): expression,
        },
    ).data
    assert expression.encode() in setup_page


COMPLEX_SCRIPT = """
y: [map (x -> (2, 3)) (if (= 3 3) then "fish" else "\n")]
# non-special comment
ops: [=, <, >, /=, >=, <=]
z: map ((x -> a -> (a..0, a.0)) 4) [(2, 3), (3, 0,), (4,2)]
# hidden [show]
foobar: 4
# not hidden [hide]
foobaz: 4
"""


def test_complex_render(client) -> None:
    upload(client, CSV_FILE, "complex_test.csv")
    setup_page = client.post("/setup", data={"script": COMPLEX_SCRIPT}).data
    assert b"fish" in setup_page
    assert setup_page.count(b"foobaz") > setup_page.count(b"foobar")
    assert b"Redirecting..." not in setup_page


def test_import_export(client) -> None:
    upload(client, CSV_FILE, "complex_test.csv")
    with BytesIO(COMPLEX_SCRIPT.encode()) as file:
        client.post("/import-upload", data={"file": (file, "complex_script.txt")})
    exported = client.post("/export", data={"filename": "a file name"})
    assert exported.data.decode() == COMPLEX_SCRIPT


def test_syntax_error(client) -> None:
    for bad_script in (
        "not valid syntax",
        'x: [3, 1.2]\nstatistics:[""]',
        "useless: 3",
        'statistics: "wrong type"',
    ):
        upload(client, CSV_FILE, "bad.csv")
        setup_page = client.post("/analysis", data={"script": bad_script}).data
        assert b"Redirecting..." in setup_page


TEST_SCRIPT = """
statistics: [string (fold + 0 (map (x -> ? 0 (parse x)) number)), "foo"]
pages: [map (= "zero") name]
"""


def test_analysis(client) -> None:
    upload(client, CSV_FILE, "analysis_test.csv")
    analysis_page = client.post("/analysis", data={"script": TEST_SCRIPT}).data
    assert b"foo" in analysis_page
    assert b"6" in analysis_page
    assert analysis_page.count(b"zero") > analysis_page.count(b"one")


CSV_EXPORT_SCRIPT = "pages: [map (x -> true) name]"


def test_csv_export(client) -> None:
    upload(client, CSV_FILE, "export_test.csv")
    client.post("/analysis", data={"script": CSV_EXPORT_SCRIPT})
    # Line endings may be different, and heading names have spaces
    # removed
    csv_lines = client.get("/analysis/0.csv").data.splitlines()
    assert csv_lines == CSV_FILE.replace(b"is prime", b"isprime").splitlines()


DATE_TIME_CSV = b"""person,birthday,birth time
Alice,2000-03-01,01:00
Bob,02/09/00,4:30 pm
Carol,3/8/1001,10:34am
Dave,4th September 3289,1:23:45 A.M.
Erin,05.04.93,03:31:49
Frank,29 Feb 2016,12:00 am
"""
DATE_TIME_SCRIPT = """
birthday_years: map (x -> ? 0 (map year (parse x))) birthday
birthday_months: map (x -> ? 0 (map month (parse x))) birthday
birthday_days: map (x -> ? 0 (map day (parse x))) birthday
birth_time_hours: map (x -> ? 0 (map hour (parse x))) birthtime
birth_time_minutes: map (x -> ? 0 (map minute (parse x))) birthtime
result: code -> ok -> & code (if ok then "OK" else "BAD")
statistics:
  [result "year" (= birthday_years [2000, 2000, 1001, 3289, 1993, 2016]),
   result "month" (= birthday_months [3, 9, 8, 9, 4, 2]),
   result "day" (= birthday_days [1, 2, 3, 4, 5, 29]),
   result "hour" (= birth_time_hours [1, 16, 10, 1, 3, 0]),
   result "minute" (= birth_time_minutes [0, 30, 34, 23, 31, 0])]
"""


def test_date_time_analysis(client) -> None:
    upload(client, DATE_TIME_CSV, "date_time_test.csv")
    analysis_page = client.post(
        "/analysis", data={"script": DATE_TIME_SCRIPT}
    ).data.decode()
    for code in ("year", "month", "day", "hour", "minute"):
        assert f"{code}OK" in analysis_page and f"{code}BAD" not in analysis_page


DUPS_CSV = b"""thing,name
CODE0,fish
CODE1,fish
CODE2,not fish
"""

DUPS_SCRIPT = """
same_name: groups [name]
pages: [duplicates same_name]
"""


def test_duplicates(client) -> None:
    upload(client, DUPS_CSV, "duplicates_test.csv")
    analysis_page = client.post("/analysis", data={"script": DUPS_SCRIPT}).data
    assert b"CODE0" in analysis_page
    assert b"CODE1" in analysis_page
    assert b"CODE2" not in analysis_page
