from datetime import date, time

from hypothesis import given, strategies as st

from analyser import analysis


@given(st.text())
def test_dates_safe(string: str) -> None:
    date_ = analysis.parse_date(string)
    assert date_ is None or (
        isinstance(date_, tuple) and len(date_) == 1 and isinstance(date_[0], date)
    )


@given(st.dates())
def test_dates_invert(date_: date) -> None:
    assert analysis.parse_date(date_.isoformat()) == (date_,)


@given(st.text())
def test_times_safe(string: str) -> None:
    time_ = analysis.parse_time(string)
    assert time_ is None or (
        isinstance(time_, tuple) and len(time_) == 1 and isinstance(time_[0], time)
    )


@given(st.times())
def test_times_invert(time_: time) -> None:
    assert analysis.parse_time(time_.isoformat()) == (time_,)
