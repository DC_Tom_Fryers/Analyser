from hypothesis import given, strategies as st

from analyser.script_edit import strings


@given(st.text(), st.text())
def test_replacements(string0: str, string1: str) -> None:
    assert (
        strings.do_replacements(string1, [(slice(0, 0), string0)]) == string0 + string1
    )
    assert (
        strings.do_replacements(string1, [(slice(0, len(string1)), string0)]) == string0
    )


@given(st.text())
def test_bracketing(string: str) -> None:
    for brackets in strings.BRACKETS:
        bracket = brackets[0]
        assert strings.in_brackets(
            strings.bracket(string, brackets=bracket), brackets=bracket
        )
