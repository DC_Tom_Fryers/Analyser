import cProfile
import io
import pstats
from pstats import SortKey

from analyser import scripts

TEST_CODE = """
f: x -> if or (= 0 x) (= 1 x) then 1 else + (f (- x 1)) (f (- x 2))
fib: map f (range 23)
dup: x -> (& x x)
x16: x -> (dup (dup (dup (dup x))))
x2048: x -> dup (dup (dup (x16 (x16 x))))
z: (map (map (* 3.1))
        (map (map (* 2.5))
             (map (map (* 0.1399))
                  (x2048
                   [[1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0],
                   [4.0, 5.0, 6.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0],
                   [4.0, 5.0, 6.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0]]))))
divides: n -> q ->
  if = q 0 then
    = n 0
  else if >= n q then
    divides (- n q) q
  else
    = 0 n
any_below_divide: p -> n ->
  if <= n 1 then
    false
  else if divides p n then
    true
  else
    any_below_divide p (- n 1)
is_prime: n -> and (>= n 2) (not (any_below_divide n (- n 1)))
primes: map is_prime (range 200)
sort_: a ->
  (? []
     (map (p ->
            & (sort_ (filter (> p) a))
              (& (filter (= p) a)
                 (sort_ (filter (< p) a))))
          (head a)))
sorted:
  (sort_ (& (reverse (range 300))
           (& [5, 1, 0, -2, 4, 3, 5, 1, 2, 3, 5]
              (range 300))))
cool_string: join " " (map case_fold (map string (range 100000)))
"""
pr = cProfile.Profile()
pr.enable()
if True:
    scripts.run_string(TEST_CODE)
else:
    scripts.parse.parse_script(TEST_CODE * 50)

pr.disable()
s = io.StringIO()
sortby = SortKey.CUMULATIVE
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())
