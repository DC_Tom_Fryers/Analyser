from __future__ import annotations

import random
from contextlib import suppress

from hypothesis import given, strategies as st
from pytest import raises

from analyser import scripts
from analyser.scripts.errors import ScriptError, ScriptNameError, ScriptRecursionError


def test_out_of_order() -> None:
    assert (
        scripts.run_string(
            """
x: y
y: 5
            """
        )["x"].value
        == 5
    )
    assert (
        scripts.run_string(
            """
x: f "foobar"
f: contains "foo"
            """
        )["x"].value
        is True
    )


def test_weird_names() -> None:
    names = ["and", "or"]
    for u in ("_" * i for i in range(10)):
        names += [
            u,
            u + "a",
            "b" + u,
            "assert" + u,
            u + "class",
            u + "c" + u,
            "r" + u,
            "pc" + u,
            "rc" + u,
            "rp" + u,
            "pr" + u,
            "False" + u,
        ]
    for name in names:
        assert (
            scripts.compile_.Compiler.decode_name(
                scripts.compile_.Compiler.encode_name(name)
            )
            == name
        )


def test_dependency_cycle() -> None:
    with raises(ScriptNameError):
        scripts.run_string("x:y\ny:x")
        scripts.run_string("x:y\ny:z\nz:x")


def test_recursion_error() -> None:
    with raises(ScriptRecursionError):
        scripts.run_string("x: y -> if true then x y else 0 \n z: x 3")


def test_comments() -> None:
    assert (
        scripts.run_string("x: 5")["x"] == scripts.run_string("x: 5 # Set x to 5")["x"]
    )
    assert (
        scripts.run_string("y: true")["y"]
        == scripts.run_string("# Set y to true\ny: true")["y"]
    )


def test_comments_parsing() -> None:
    statements = scripts.parse.parse_script("# Foo").statements
    assert len(statements) == 1
    assert isinstance(statements[0], scripts.nodes.Comment)
    assert statements[0].content == " Foo"

    statements = scripts.parse.parse_script("x: 3\n# bar\nz: 4").statements
    assert len(statements) == 3
    assert isinstance(statements[1], scripts.nodes.Comment)
    assert statements[1].content == " bar"


@given(st.text())
def test_random_strings(code: str) -> None:
    with suppress(ScriptError):
        scripts.run_string(code)
    with suppress(ScriptError):
        scripts.run_string(f"var_name:{code}")


def test_random_codelike_strings() -> None:
    code_bits = tuple(scripts.builtin.BUILTIN_NAMES) + (
        "(",
        ")",
        "->",
        "if",
        "then",
        "else",
        ":",
        ",",
        "[",
        "]",
        "\n",
        "0",
        "1",
        "2",
        "0.0",
        "1.0",
        "-1.0",
        "2.0",
        "x",
        "y",
        "z",
        '"foo"',
        '"bar"',
        '""',
    )
    for _ in range(2000):
        code = " ".join(
            random.choice(code_bits) for _ in range(random.randrange(2, 10))
        )
        with suppress(ScriptError):
            scripts.run_string(f"var_name:{code}")


def test_polymorphic_function_aliases() -> None:
    scripts.run_string(
        """
y: reduce (zip_with +) [[1, 2, 3], [4, 5, 6], [5, 4, 2]]
        """
    )
    scripts.run_string(
        """
x: zip_with
y: reduce (x +) [[1, 2, 3], [4, 5, 6], [5, 4, 2]]
        """
    )


@given(st.text())
def test_string_escaping(string_: str) -> None:
    assert scripts.parse.unescape(scripts.parse.escape(string_)) == string_
