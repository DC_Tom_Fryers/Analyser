import math
import random

from hypothesis import given, strategies as st
from pytest import approx, raises

from analyser import scripts
from analyser.scripts.errors import ScriptParseError, ScriptTypeError
from tests.scripts import bad_types, eval_, no_type, random_int, random_number


def test_arithmetic() -> None:
    assert eval_("+ 1 1") == 2
    assert eval_("+ 1.5 -1.1") == approx(0.4)
    assert eval_("* -5 -10") == 50
    assert eval_("- 5.3 10.3") == approx(-5.0)
    assert eval_("- 8 19") == -11

    bad_types("- 5 2.6", "* [1, 2] 3", '+ "foo" "bar"', "+ (some 2) (some 3)", "+ -")


@given(st.integers())
def test_number_relations(number: int) -> None:
    assert eval_(f"= {number} {number}") is True
    assert eval_(f'= ({number}, "foo") ({number}, "foo")') is True
    assert (
        eval_(
            f'= (true, {number}, "foo", (1, 2, 5), ["foo", "bar"])'
            f' (false, {number}, "foo", (1, 2, 5), ["foo", "bar"])'
        )
        is False
    )
    assert eval_(f"= [{number}, {number}] [{number}, {number}]") is True
    assert eval_(f"/= {number} {number}") is False
    assert eval_(f"< {number} {number}") is False
    assert eval_(f"> {number} {number}") is False
    assert eval_(f"<= {number} {number}") is True
    assert eval_(f">= {number} {number}") is True


def test_string_relations() -> None:
    for test_str in ("foo", "bar", "\N{OX}", "", "\\\\", "\\n"):
        assert eval_(f'= "{test_str}" "{test_str}"') is True
        assert eval_(f'contains "{test_str}" "{test_str}"') is True
        assert eval_(f'contains "{test_str}" "{test_str}more_chars"') is True
        assert eval_(f'contains "surrounded{test_str}ofthings" "{test_str}"') is False


def test_boolean_relations() -> None:
    assert eval_("= true true") is True
    assert eval_("= true false") is False
    assert eval_("= false true") is False
    assert eval_("= false false") is True

    assert eval_("/= true true") is False
    assert eval_("/= true false") is True
    assert eval_("/= false true") is True
    assert eval_("/= false false") is False


def test_bad_comparisons() -> None:
    bad_types(
        "= 8 8.0",
        "/= true 1",
        '= 2 "2"',
        "= [true, false] (true, false)",
        "< (some 2) none",
        "> [2] [3]",
        ">= (2, 5) (5, 2)",
    )


def test_boolean() -> None:
    assert eval_("or true true") is True
    assert eval_("or false true") is True
    assert eval_("or true false") is True
    assert eval_("or false false") is False

    assert eval_("and true true") is True
    assert eval_("and false true") is False
    assert eval_("and true false") is False
    assert eval_("and false false") is False

    assert eval_("not true") is False
    assert eval_("not false") is True

    bad_types("or 1 1", "and true 2", "not none")


def test_arrays() -> None:
    assert eval_("[3, 4, 5]") == eval_("[3, 4, 5,]") == (3, 4, 5)
    assert eval_('["foo", "bar"]') == ("foo", "bar")
    assert eval_('[["foo", "bar"], ["foobar", "barfoo"]]') == (
        ("foo", "bar"),
        ("foobar", "barfoo"),
    )
    assert eval_("[false, true, false, false]") == (False, True, False, False)
    assert eval_("[-1.4]") == (-1.4,)

    bad_types("[3, 2.1]", "[[2], 9]", "[some 5, some 5.0]")
    no_type("[]", "[[]]")


def test_tuples() -> None:
    assert eval_("(1, 5.2)") == eval_("(1, 5.2,)") == (1, 5.2)
    assert eval_('(true, false, 1, 2, 3, "string", [1, 2, 3], (5.0, 6.0))') == (
        True,
        (False, (1, (2, (3, ("string", ((1, 2, 3), (5.0, 6.0))))))),
    )
    assert eval_('("foo", 3).0') == "foo"
    assert eval_('("bar", 9.2, 2).1') == 9.2
    assert eval_("(x -> x.0) (1, 2)") == 1
    assert eval_("= (1, 2, 3) (1, (2, 3))")
    assert eval_('("a", "b", "c", "d").0') == "a"
    assert eval_('("a", "b", "c", "d").1') == "b"
    assert eval_('("a", "b", "c", "d").2') == "c"
    assert eval_('("a", "b", "c", "d")..0') == ("b", ("c", "d"))
    assert eval_('("a", "b", "c", "d")..1') == ("c", "d")
    assert eval_('("a", "b", "c", "d")..2') == "d"
    assert eval_("map (y -> y.5) [(2.3, 4.1, 4.5, -9.2, 1.2, 1.2, 1.0)]") == (1.2,)
    bad_types("(x -> x.3) (1, 2)", "(1, 2).1")
    with raises(ScriptParseError):
        eval_("()")
        eval_("(3,)")


def test_option() -> None:
    assert eval_('some "foo"') == ("foo",)
    assert eval_("= none none") is True
    assert eval_("= none (some 5)") is False
    # REVIEW This test is odd. Maybe it should fail.
    assert eval_("= none (some none)") is False
    assert eval_("= (some true) (some true)") is True
    assert eval_("? 5 none") == 5
    assert eval_("? -3.5 (some 2.1)") == 2.1

    bad_types("= none true", f"? {random_number()} {random_number()}")
    no_type("none", "some none")


def test_mapping() -> None:
    assert eval_("map (+ 3) [1, 2, 3]") == (4, 5, 6)
    assert eval_('map (contains "foo") ["foo", "bar", "foobar"]') == (True, False, True)
    assert eval_("map not [false, true]") == (True, False)
    assert eval_("map any [[false, false], [true, false]]") == (False, True)
    assert eval_("(? none (head (map (map +) [none, some 2])))") is None
    assert eval_("map (+ 2) (some 3)") == (5,)

    bad_types(
        f"map {random_number()} []",
        f"map (+ {random_int()}) [5.2]",
        "map map map",
        "map (- 3) (1, 3)",
    )
    no_type("map some []")


def test_join() -> None:
    assert eval_('join "" ["concatenate", " ", "these"]') == "concatenate these"
    assert (
        eval_(
            """
(join " "
      (map (join " ")
           [["joining", "nested", "arrays"], ["of", "strings"]]))
            """
        )
        == "joining nested arrays of strings"
    )
    assert eval_('join " " []') == ""
    bad_types('join "" [1, 2, 3]', 'join 5 ["hello", "hi"]')


def test_ifte() -> None:
    assert eval_("if true then 2 else 3") == 2
    assert eval_("if false then (1, 2) else (3, 4)") == (3, 4)
    bad_types(
        "if [] then 2 else 3",
        'if 0 then "foo" else "bar"',
        "if none then true else false",
        "if true then 5 else 4.3",
        "if false then 5 else none",
    )
    no_type("if false then [] else []")


def test_filter() -> None:
    assert eval_("filter (< 3) [1, 2, 9, 104, 7, -7]") == (9, 104, 7)
    assert (
        eval_(
            """
(filter
 (y -> and (not (contains "bar" y))
           (/= "foo" y))
 ["foo", "bar", "spam", "bars", "foo", "eggs"])
            """
        )
        == ("spam", "eggs")
    )

    bad_types(
        "filter (+ 2) [2, 3, 5]", "filter (= 5) [5.2, 5.2]", 'filter (= "foo") "foo"'
    )


def test_fold() -> None:
    assert eval_("fold + 0 [1, 3, 6, 9, 9, 12, -19]") == 21
    assert eval_(f"reduce * (tail [{random_number()}])") is None
    assert eval_("reduce * [2.0, 4.0, 1.0]") == (8.0,)

    assert eval_("reduce + [1.3, 9.4, -2.3]")[0] == approx(8.4)

    bad_types(
        "fold + 2 [1.0]",
        "fold + + +",
        "fold (+ 2) 1 [1, 9]",
        "reduce + [true, false]",
        "reduce - [false]",
    )


def test_head() -> None:
    assert eval_("head [5, 4, 3]") == (5,)
    assert eval_("head [-21.9]") == (-21.9,)
    bad_types("head 5", "head tail", "head (5, 2)")
    no_type("head []")


def test_tail() -> None:
    assert eval_("tail [5, 4, 3]") == (4, 3)
    assert eval_("tail [-21.9]") == ()
    bad_types(
        f"tail {random_number()}",
        "tail head",
        f"tail ({random_number()}, {random_number()})",
    )
    no_type("tail []")


def test_id() -> None:
    assert eval_("id true") is True
    assert eval_("id 9.5") == 9.5
    assert eval_("id id id 8") == 8


def test_zip() -> None:
    assert eval_('zip [0, 1, 2, 3] ["a", "b", "c"]') == ((0, "a"), (1, "b"), (2, "c"))
    assert eval_("zip_with + [1, 2, 3] [4, 4, 9]") == (5, 6, 12)
    assert eval_("zip_with + [1, 2, 3] [4, 4, 9, 4]") == (5, 6, 12)

    bad_types(
        "zip (1, 3) (5, 3)", "zip_with [1, 2] [4, 5]", "zip_with + [1, 0] [0.2, 3.4]"
    )


def test_index() -> None:
    assert eval_("index [5.2, 3.1, 5.9] 2") == (5.9,)
    assert eval_(f"index [{random_number()}] 1") is None

    bad_types("index 3 2", "index 1 [2, 3]", "index (1, 2) 1")


def test_in() -> None:
    assert eval_("in 9 [1, 2, 3]") is False
    assert eval_("in 5.2 [1.5, 2.1, 5.2]") is True
    bad_types("in 3 [true]", "in 5 (5, 2)")


def test_concatenation() -> None:
    assert eval_('& "foo" "bar"') == "foobar"
    with raises(ScriptTypeError):
        scripts.run_string(
            """
ruin_string: a -> ? [] (map (x -> [x]) (head a))
concat_2: a -> & (? [] (head a)) (? [] (index a 1))
bad: x -> y -> concat_2 (& [x] (map ruin_string [y]))
ok: bad [1] [2]
broken: bad "foo" "bar"
            """
        )
    assert eval_("& [1, 2] [3, 4]") == (1, 2, 3, 4)
    bad_types(
        '& "foo" ["b", "a", "r"]',
        "& [1] [1.0]",
        "& none none",
        'map (x -> & [x] "bar") (head "foo") ',
        '& (? [] (map (x -> [x]) (head "foo"))) "bar"',
    )


def test_length() -> None:
    assert eval_(f"length [{random_int()}, {random_int()}, {random_int()}]") == 3
    assert eval_('length "foobar"') == 6
    bad_types("length (1, 2)", f"length {random_number()}")


def test_division() -> None:
    assert eval_("/ 10.0 2.0") == 5.0
    assert eval_("/ 1.0 0.0") == math.inf
    assert eval_("/ 1.0 -0.0") == -math.inf
    assert math.isnan(eval_("/ 0.0 -0.0"))
    assert eval_("/ 0.0 -1.0") == 0.0
    bad_types("/ 10 2", "/ / /", "/ 5.0 2")


def test_count() -> None:
    assert eval_("count (> 2) [0,1,2,3]") == 2
    assert eval_("count (= (true, false)) [(true, false), (false, false)]") == 1
    bad_types("count = [1, 2, 3]", "count (< 5) (1, 2)")


def test_unique() -> None:
    assert eval_("unique [1, 2, 1, 1, 0, 1, 2, 0, 1]") == (1, 2, 0)
    bad_types("unique 3")


def test_take() -> None:
    assert eval_("take 3 [5, 1, 4, 1]") == (5, 1, 4)
    assert eval_("take 0 [1, 2, 2]") == ()
    assert eval_("take 9 [4, 1, -1]") == (4, 1, -1)
    bad_types("take 3 (2, 2)")


def test_drop() -> None:
    assert eval_("drop 3 [5, 1, 4, 1]") == (1,)
    assert eval_("drop 0 [1, 2, 2]") == (1, 2, 2)
    assert eval_("drop 9 [4, 1, -1]") == ()
    bad_types("drop 3 (2, 2)")


def test_range() -> None:
    assert eval_("range 0") == eval_("range -1") == ()
    assert eval_("range 1") == (0,)
    assert eval_("range 2") == (0, 1)
    assert eval_("range 3") == (0, 1, 2)
    bad_types("range 3.0")


def test_parse() -> None:
    assert eval_('? 2.3 (parse "-1.2")') == -1.2
    assert eval_('? 0 (parse "500")') == 500
    assert eval_('? 0 (parse "500---")') == 0
    assert eval_('? false (parse "true")') is True
    assert eval_('? false (parse "fish")') is False
    assert eval_('? true (parse "False")') is True
    bad_types('? "fish" (parse "foo")')


def test_string() -> None:
    assert eval_("string -5") == "-5"
    assert eval_("string (+ 5.0 3.5)") == "8.5"
    bad_types("string (1, 2)")


def test_reverse() -> None:
    assert eval_("reverse [1, 2, 3]") == (3, 2, 1)
    assert eval_("reverse (tail [1])") == ()
    assert eval_('reverse "foo"') == "oof"
    assert eval_('reverse "foo"') == "oof"
    end = random.randrange(100)
    assert eval_(f"= (- {end} 1) (? -1 (head (reverse (range {end}))))") is True


def test_mod() -> None:
    assert eval_("mod 3 2") == (1,)
    assert eval_("mod 3 0") is None
    assert eval_("mod 100 1000") == (100,)


def test_sqrt() -> None:
    assert eval_("sqrt 4.0") == 2.0
    assert math.isnan(eval_("sqrt -1.0"))
    assert str(eval_("sqrt -0.0")) == "-0.0"
    assert str(eval_("sqrt 0.0")) == "0.0"


def test_exp() -> None:
    assert eval_("exp 1.0") == math.e
    assert eval_("exp 0.0") == 1.0


def test_to_float() -> None:
    for _ in range(100):
        number = random_int()
        assert eval_(f"float {number}") == float(number)
    assert eval_(f"float 1{'0' * 1000}") == math.inf
    assert eval_(f"float -1{'0' * 1000}") == -math.inf


def test_sort() -> None:
    assert eval_("sort [3, 5, 2, 1, 5, 4]") == (1, 2, 3, 4, 5, 5)
    assert eval_('join "" (sort ["a", "d", "c", "a", "c", "z"])') == "aaccdz"
    bad_types("sort [[1, 2], [3, 4]]")


def test_sort_by() -> None:
    assert eval_("sort_by (x -> (+ x.0 x..0)) [(1, 5), (1, 1), (2, 1)]") == (
        (1, 1),
        (2, 1),
        (1, 5),
    )
    bad_types("sort_by 5 2")
