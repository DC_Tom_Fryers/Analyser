from __future__ import annotations

import random
from typing import Any

from pytest import raises

from analyser import scripts
from analyser.scripts.errors import ScriptTypeError


def eval_(expr: str) -> Any:
    return scripts.run_string(f"EVAL_VAR: {expr}")["EVAL_VAR"].value


def type_(expr: str) -> scripts.types.Type:
    return scripts.run_string(f"EVAL_VAR: {expr}")["EVAL_VAR"].type_


def bad_types(*expressions: str) -> None:
    for expression in expressions:
        with raises(ScriptTypeError):
            eval_(expression)


def no_type(*expressions: str) -> None:
    for expression in expressions:
        assert "EVAL_VAR" not in scripts.run_string(f"EVAL_VAR: {expression}")


def random_int() -> int:
    size = (100, 100000000000, 10 ** 100)[random.randrange(3)]
    return random.randint(-size, size)


def random_float() -> str:
    size = (1e-100, 0.00001, 10, 100000000000, 1e100)[random.randrange(5)]
    return scripts.builtin.string_float(random.uniform(-size, size))


def random_number() -> str:
    if random.randrange(2):
        return str(random_int())
    return random_float()
