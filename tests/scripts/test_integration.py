from __future__ import annotations

from hypothesis import given, strategies as st

from analyser import scripts
from tests.scripts import eval_


def test_fibonacci() -> None:
    assert (
        scripts.run_string(
            """
f: x -> if or (= 0 x) (= 1 x) then 1 else + (f (- x 1)) (f (- x 2))
fib: map f [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            """
        )["fib"].value
        == (1, 1, 2, 3, 5, 8, 13, 21, 34, 55)
    )


def test_ackermann() -> None:
    assert (
        scripts.run_string(
            """
a: m -> n ->
  if = 0 m then
    + 1 n
  else if = 0 n then
    a (- m 1) 1
  else
    a (- m 1) (a m (- n 1))
x: map (i -> map (a i) (range 7)) (range 4)
            """
        )["x"].value
        == (
            (1, 2, 3, 4, 5, 6, 7),
            (2, 3, 4, 5, 6, 7, 8),
            (3, 5, 7, 9, 11, 13, 15),
            (5, 13, 29, 61, 125, 253, 509),
        )
    )


def test_primes() -> None:
    assert (
        scripts.run_string(
            """
divides: n -> q ->
  if = q 0 then
    = n 0
  else if >= n q then
    divides (- n q) q
  else
    = 0 n
any_below_divide: p -> n ->
  if <= n 1 then
    false
  else if divides p n then
    true
  else
    any_below_divide p (- n 1)
is_prime: n -> and (>= n 2) (not (any_below_divide n (- n 1)))
primes: map is_prime [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            """
        )["primes"].value
        == (False, False, True, True, False, True, False, True, False, False)
    )


@given(st.lists(st.integers(), min_size=1))
def test_sorting(ints: list[int]) -> None:
    code = f"""
sort_: a ->
  (? []
     (map (p -> &
                (sort_ (filter (> p) a))
                (& (filter (= p) a)
                   (sort_ (filter (< p) a))))
          (head a)))
x: sort_ {ints}
           """
    assert scripts.run_string(code)["x"].value == tuple(sorted(ints))


def test_flip() -> None:
    scripts.run_string(
        """
flip: f -> a -> b -> (f b a)
ab: map (flip (flip in) "fish") [["fishes"]]
        """
    )


def test_general_conditioning() -> None:
    assert (
        eval_(
            """
(zip_with
 (x -> y ->
   +
   (if x then 1 else 0)
   (if y then 1 else 0))
 [false, true, true]
 [false, false, true])
            """
        )
        == (0, 1, 2)
    )


@given(st.lists(st.integers(), min_size=1), st.lists(st.integers(), min_size=1))
def test_alternative_zip_with(numbers0: list[int], numbers1: list[int]) -> None:
    variables = scripts.run_string(
        f"""
numbers: ({numbers0}, {numbers1})
zip_with_: f -> a -> b -> map (x -> f x.0 x..0) (zip a b)
norm: zip_with + numbers.0 numbers..0
alt: zip_with_ + numbers.0 numbers..0
        """
    )
    assert (
        variables["norm"].value
        == variables["alt"].value
        == tuple(map(lambda x, y: x + y, numbers0, numbers1))
    )


def test_map_double_option() -> None:
    scripts.run_string(
        """
a: x -> ? false (map (? false) x)
b: map a [(some (some true))]
        """
    )
