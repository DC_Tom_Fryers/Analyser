from pytest import raises

from analyser import scripts
from analyser.scripts.errors import (
    ScriptNameError,
    ScriptParseError,
    ScriptTokenisationError,
    ScriptTypeError,
)
from tests.scripts import eval_


def test_parse_errors() -> None:
    for expression in ('"', "!", ";aj;@jad'fj`9`"):
        with raises(ScriptTokenisationError):
            eval_(expression)
    for expression in (
        ":",
        "x: 5",
        "5 -> 3",
        "if true then if 3",
        "if",
        "then if else",
        "if then else",
        "x ->",
        "-> 3",
        "(",
        ")",
        "[",
        "]",
        "(]",
        "[)",
        ",",
    ):
        with raises(ScriptParseError):
            eval_(expression)


def test_name_errors() -> None:
    with raises(ScriptNameError):
        eval_("jlkdlfadfkjadfldfdsjf")

    with raises(ScriptNameError):
        eval_("jasdfasl -> jasdfasl abdsdfafdadlfkkj")

    with raises(ScriptNameError):
        eval_("+ 5 may_be_a_number_alksndk")


def test_name_collisions() -> None:
    with raises(ScriptNameError):
        scripts.run_string(
            """
x: 5
y: x -> 3
z: y 2
            """
        )
    with raises(ScriptNameError):
        scripts.run_string(
            """
x: 5
x: 6
            """
        )


def test_recursion_type_error() -> None:
    with raises(ScriptTypeError):
        scripts.run_string("f: x -> some (if = 0 x then f (- x 1) else 0)")
